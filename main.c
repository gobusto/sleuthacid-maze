/**
@mainpage SLEUTHACID MAZE

@section sec_howto How To Play

Use the white player icon to push the red rocks into yellow holes.

@section sec_controls Controls

WASD:   Move
R:      Restart
Escape: Quit

@section sec_credits Credits

LEAD PROGRAMMER: Edward F. Biddulph
PROGRAMMER:      Thomas Glyn Dennis
TESTING:         Mathew Carr
*/

/**
@file main.c

@brief The program start point.

This file simply contains the main() function called when the program begins.
*/

#include <stdlib.h>
#include "core/qqqSystem.h"

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char *argv[])
{
  if (sysInit(argc, argv) == 0) { sysMainLoop(); }
  sysQuit();
  return 0;
}
