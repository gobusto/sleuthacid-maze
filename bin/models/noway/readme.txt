AUTHORS: noway
ADDRESS: http://opengameart.org/content/camel-basemesh
LICENSE: CC0 (Public Domain)

A generic camel mesh. It is not gameready but may serve as a starting point for
further modifications.

Approximately 800 tris. Made in Wings3d.
