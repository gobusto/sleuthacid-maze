AUTHORS: Thomas Glyn Dennis
ADDRESS: https://gitlab.com/gobusto/sleuthacid-maze
LICENSE: CC0 (Public Domain)

This is the sprite sheet used to draw the world and score.
