/**
@file qqqWorld.h

@brief Contains the main game logic functionality.

All game logic, physics, etc. belongs here.
*/

#ifndef __QQQ_WORLD_H__
#define __QQQ_WORLD_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates possible kinds of tile.

Note that this does not include a "player" tile; the player is drawn on top of
the tile grid in order to prevent them from overwriting "hole" tiles when they
walk over them.
*/

typedef enum
{

  SAM_TILE_NONE,  /**< @brief An empty tile. Players/rocks can move over it. */
  SAM_TILE_WALL,  /**< @brief An impassible tile. Players/rocks are blocked. */
  SAM_TILE_ROCK,  /**< @brief A movable rock. Players can push these around. */
  SAM_TILE_HOLE,  /**< @brief Hole in ground. Players push rocks into these. */
  SAM_NUM_TILES   /**< @brief Gives the total possible number of tile types. */

} sam_tile_t;

/**
@brief Generate a new world.

@todo Possibly allow Width and Height to be specified as parameters?

@return Zero on success, or non-zero on failure.
*/

int worldInit(void);

/**
@brief Free any currently existing world data.

This is automatically called by worldInit() just in case data already exists.
*/

void worldQuit(void);

/**
@brief Move the player character.

@param xdelta The number of X tiles to move, relative to the current position.
@param ydelta The number of Y tiles to move, relative to the current position.
*/

void worldMovePlayer(int xdelta, int ydelta);

/**
@brief Get the width of the currently active world in tiles.

@return The width of the world in tiles.
*/

int worldGetWidth(void);

/**
@brief Get the height of the currently active world in tiles.

@return The height of the world in tiles.
*/

int worldGetHeight(void);

/**
@brief Get the tile at the coordinates specified.

@param x The X position of the tile.
@param y The Y position of the tile.
@return The type of tile at the coordinates specified.
*/

sam_tile_t worldGetTile(int x, int y);

/**
@brief Get the player X position in tiles.

@return The player X position.
*/

int worldGetX(void);

/**
@brief Get the player Y position in tiles.

@return The player Y position.
*/

int worldGetY(void);

/**
@brief Get the current score.

@return The current score.
*/

int worldGetScore(void);

#ifdef __cplusplus
}
#endif

#endif  /* __QQQ_WORLD_H__ */
