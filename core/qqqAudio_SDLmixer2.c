/**
@file qqqAudio_SDLmixer2.c

@brief Contains sound-related functionality.

This file implements the functions declared in qqqAudio.h using SDL2_mixer.
*/

#include <SDL2/SDL_mixer.h>
#include <stdlib.h>
#include "qqqAudio.h"

/* Global objects and variables. */

SDL_bool snd_isinit = SDL_FALSE;  /**< @brief Initialisation flag. */

Mix_Music *snd_music = NULL;  /**< @brief Used to store the current music. */

Mix_Chunk *snd_goal = NULL; /**< @brief Played when a rock drops into a hole. */
Mix_Chunk *snd_warp = NULL; /**< @brief Played when a new world is generated. */

/*
[PUBLIC] Initialise the audio subsystem.
*/

int sndInit(void)
{

  sndQuit();

  snd_isinit = !Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
  if (!snd_isinit) { return -1; }

  snd_goal = Mix_LoadWAV("sounds/farfadet46/pop.wav");
  snd_warp = Mix_LoadWAV("sounds/DaGameKnower/robot_0.wav");

  return 0;

}

/*
[PUBLIC] Shut down the audio subsystem.
*/

void sndQuit(void)
{

  if (!snd_isinit) { return; }

  sndStopMusic();
  Mix_HaltChannel(-1);

  Mix_FreeChunk(snd_goal); snd_goal = NULL;
  Mix_FreeChunk(snd_warp); snd_warp = NULL;

  Mix_CloseAudio();

}

/*
[PUBLIC] Play a sound effect.
*/

void sndPlayEffect(snd_effect effect_id)
{

  if (!snd_isinit) { return; }

  switch (effect_id)
  {
    case SND_EFFECT_GOAL: Mix_PlayChannel(-1, snd_goal, 0); break;
    case SND_EFFECT_WARP: Mix_PlayChannel(-1, snd_warp, 0); break;
    case SND_NUM_EFFECTS: default: break;
  }

}

/*
[PUBLIC] Play (and loop) a music track.
*/

int sndPlayMusic(const char *file_name)
{

  if (!snd_isinit) { return 1; }

  sndStopMusic();

  if (!file_name) { return -1; }
  snd_music = Mix_LoadMUS(file_name);
  if (!snd_music) { return -2; }
  if (Mix_PlayMusic(snd_music, -1) != 0) { return -3; }

  return 0;

}

/*
[PUBLIC] Stop any currently playing music.
*/

void sndStopMusic(void)
{

  if (!snd_isinit) { return; }

  Mix_HaltMusic();
  Mix_FreeMusic(snd_music);
  snd_music = NULL;

}

/*
[PUBLIC] Returns non-zero if music is currently playing.
*/

int sndMusicPlaying(void) { return snd_music != NULL; }
