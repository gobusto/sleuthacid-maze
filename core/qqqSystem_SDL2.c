/**
@file qqqSystem_SDL2.c

@brief Contains things related to window management, etc.

This file implements the functions declared in qqqSystem.h using SDL2.
*/

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "qqqAudio.h"
#include "qqqGraphics.h"
#include "qqqSystem.h"
#include "qqqWorld.h"

/* Global objects and variables. */

SDL_Window    *sys_window  = NULL;  /**< @brief The main SDL program window. */
SDL_GLContext *sys_context = NULL;  /**< @brief An OpenGL rendering context. */

/*
[PUBLIC] Initialise everything.
*/

int sysInit(int argc, char *argv[])
{

  const char *title = NULL;
  const int w = 544;
  const int h = 576;

  int i = 0;

  SDL_bool audio_enabled = SDL_TRUE;

  /* If the system has already been initialised, shut eveything down first. */

  sysQuit();

  /* Handle any command-line arguments. */

  for (i = 1; i < argc; ++i)
  {
    if (strcmp(argv[i], "--noaudio") == 0) { audio_enabled = SDL_FALSE; }
  }

  /* Set up the random seed value and choose a random title string. */

  srand((unsigned int)time(NULL) * 3456);

  switch (rand() % 7)
  {
    case 0:  title = "Sleuthacid Maze - bol is bleeding";              break;
    case 1:  title = "Sleuthacid Maze - this wizraz is nice and cool"; break;
    case 2:  title = "Sleuthacid Maze - are you a mushroom? N/A";      break;
    case 3:  title = "Sleuthacid Maze - connecting camels";            break;
    case 4:  title = "Sleuthacid Maze - easy as cheesy";               break;
    case 5:  title = "Sleuthacid Maze - garfield, why am i so bald?";  break;
    default: title = "Sleuthacid Maze - sponsored by rocketbattle.bb"; break;
  }

  /* Initialise SDL. */

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't initialise SDL.", NULL);
    return -1;
  }

  /* Create an OpenGL window. */

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
#if 0
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
#endif
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);

  sys_window = SDL_CreateWindow(title, 40, 40, w, h, SDL_WINDOW_OPENGL);
  if (!sys_window)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't create a window.", NULL);
    return -2;
  }

  sys_context = SDL_GL_CreateContext(sys_window);
  if (!sys_context)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't create an OpenGL context.", NULL);
    return -3;
  }

  /* Initialise the graphics subsystem. */

  if (gfxInit(w, h) != 0)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't initialise the graphics subsystem.", NULL);
    return -4;
  }

  /* Initialise the audio subsystem. */

  if (audio_enabled)
  {

    if (sndInit() != 0)
    {
      SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING,
          "Warning", "Couldn't initialise the audio subsystem.", NULL);
    }

    sndPlayMusic("music/music.ogg");

  }

  /* Generate a random world. */

  if (worldInit() != 0)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't generate a world.", NULL);
    return -5;
  }

  /* Success! */

  return 0;

}

/*
[PUBLIC] Shut everything down again.
*/

void sysQuit(void)
{

  if (!SDL_WasInit(0)) { return; }

  worldQuit();

  sndQuit();
  gfxQuit();

  SDL_GL_DeleteContext(sys_context);
  sys_context = NULL;

  SDL_DestroyWindow(sys_window);
  sys_window = NULL;

  SDL_Quit();

}

/*
[PUBLIC] Enter the main game loop.
*/

void sysMainLoop(void)
{

  SDL_Event event;

  while (SDL_TRUE)
  {

    /* Handle any system events. */

    while (SDL_PollEvent(&event))
    {

      if (event.type == SDL_KEYDOWN)
      {

        switch (event.key.keysym.sym)
        {

          case SDLK_r:
            if (!event.key.repeat)
            {
              if (worldInit() != 0)
              {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                    "Error", "Couldn't generate a world.", NULL);
                return;
              }
            }
          break;

          case SDLK_w: worldMovePlayer( 0, -1); break;
          case SDLK_s: worldMovePlayer( 0, +1); break;
          case SDLK_a: worldMovePlayer(-1,  0); break;
          case SDLK_d: worldMovePlayer(+1,  0); break;

          case SDLK_ESCAPE: return;
          default:          break;

        }

      } else if (event.type == SDL_QUIT) { return; }

    }

    /* Redraw the display. */

    gfxDraw(SDL_GetTicks());
    SDL_GL_SwapWindow(sys_window);

    SDL_Delay(1); /* <-- Thread sleep (prevents constant 100% CPU usage). */

  }

}
