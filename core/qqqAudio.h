/**
@file qqqAudio.h

@brief Contains sound-related functionality.

All sound effect and music functions belong here.
*/

#ifndef __QQQ_AUDIO_H__
#define __QQQ_AUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates possible sound effects.

These values are passed to sndPlayEffect().
*/

typedef enum
{

  SND_EFFECT_GOAL,  /**< @brief Played when a rock is dropped into a hole.  */
  SND_EFFECT_WARP,  /**< @brief Played when a new level begins.             */
  SND_NUM_EFFECTS   /**< @brief The total number of possible sound effects. */

} snd_effect;

/**
@brief Initialise the audio subsystem.

@return Zero on success, or non-zero on failure.
*/

int sndInit(void);

/**
@brief Shut down the audio subsystem.

This is called automatically by sysQuit().
*/

void sndQuit(void);

/**
@brief Play a sound effect.

@param effect_id The type of effect to play.
*/

void sndPlayEffect(snd_effect effect_id);

/**
@brief Play (and loop) a music file.

Any currently-playing music is stopped first.

@param file_name The name (and path) of the music file to play.
@return Zero on success or non-zero on error.
*/

int sndPlayMusic(const char *file_name);

/**
@brief Stop any currently-playing music.

Does what you'd expect it to do.
*/

void sndStopMusic(void);

/**
@brief Determine if any music is currently being played.

@return Non-zero if music is being currently being played, or zero if not.
*/

int sndMusicPlaying(void);

#ifdef __cplusplus
}
#endif

#endif  /* __QQQ_AUDIO_H__ */
