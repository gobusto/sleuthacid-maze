/**
@file qqqSystem.h

@brief Contains things related to window management, etc.

The functions in this file handle the initialisation, running, and shutdown of
all other subsystems (graphics, audio, etc.) in the game, so it can be thought
of as the "manager" of the program.
*/

#ifndef __QQQ_SYSTEM_H__
#define __QQQ_SYSTEM_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise everything.

Call this first.

@param argc Length of the argv array.
@param argv List of program argument strings.
@return Zero on success, or non-zero on failure.
*/

int sysInit(int argc, char *argv[]);

/**
@brief Shut everything down again.

Call this just before the progam ends.
*/

void sysQuit(void);

/**
@brief Enter the main game loop.

This will run until a termination event occurs.
*/

void sysMainLoop(void);

#ifdef __cplusplus
}
#endif

#endif  /* __QQQ_SYSTEM_H__ */
