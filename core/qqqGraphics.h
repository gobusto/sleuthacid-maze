/**
@file qqqGraphics.h

@brief Contains graphics-related functionality.

All 2D and 3D rendering functions belong here.
*/

#ifndef __QQQ_GRAPHICS_H__
#define __QQQ_GRAPHICS_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise the graphics subsystem.

@param w The display width in pixels.
@param h The display height in pixels.
@return Zero on success, or non-zero on failure.
*/

int gfxInit(int w, int h);

/**
@brief Shut down the graphics subsystem.

This is called automatically by sysQuit().
*/

void gfxQuit(void);

/**
@brief Redraw the display.

Note that this does not swap the front/back buffers, in case something needs to
be drawn in front of the in-game display (such as a pause menu).

@param ticks The current time in milliseconds. Used for animations.
*/

void gfxDraw(double ticks);

#ifdef __cplusplus
}
#endif

#endif  /* __QQQ_GRAPHICS_H__ */
