/*

FILENAME: mtrl.h
AUTHOR/S: Thomas Dennis
CREATION: 25th December 2010

Copyright (c) 2010-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

#ifndef __QQQ_MATERIAL_H__
#define __QQQ_MATERIAL_H__

/**
@file mtrl.h

@brief Provides utilities relating to mesh material information.

Note that this file does not load textures; it only handles "descriptive" data,
such as the MTLLIB files used by OBJ meshes.
*/

#ifdef __cplusplus
extern "C" {
#endif

/* Define constant values. */

#define MTRL_MAXSTRLEN 128  /**< @brief Maximum static string length. */

/**
@brief Defines a material.

Writeme
*/

typedef struct
{

  char name[MTRL_MAXSTRLEN];        /**< @brief Material name.             */

  char diffusemap[MTRL_MAXSTRLEN];  /**< @brief Diffuse texture filename.  */
  char bumpmap[MTRL_MAXSTRLEN];     /**< @brief Bump-map texture filename. */
  char alphamap[MTRL_MAXSTRLEN];    /**< @brief Transparency texture file. */

  float alpha;                      /**< @brief Transparency value.        */

} mtrl_s;

/**
@brief Stores a group of materials.

This structure mainly exists to make loading multiple material definitions
(i.e. loading material lists from files) more convenient.
*/

typedef struct
{

  long   count;   /**< @brief Number of list entries. */
  mtrl_s **mtrl;  /**< @brief List of materials. */

} mtrl_list_s;

/**
@brief Create a new Material List.

Does what you'd expect.

@param size about
@return about
*/

mtrl_list_s *mtrlCreateList(long size);

/**
@brief Free a previously created Material List.

Does what you'd expect.

@param list The list to be freed.
@return Always returns NULL.
*/

mtrl_list_s *mtrlFreeList(mtrl_list_s *list);

/**
@brief Find a material (by name).

writeme

@param list The list to search in.
@param name The name to search for.
@return A valid index if the material is found, otherwise returns -1.
*/

long mtrlFind(const mtrl_list_s *list, const char *name);

/**
@brief about

about

@param text about
@return about
*/

mtrl_list_s *mtrlLoadText(const char *text);

/**
@brief about

about

@param file_name about
@return about
*/

mtrl_list_s *mtrlLoadFile(const char *file_name);

/**
@brief Export a Material List as a Wavefront MTL file.

These files are usually associated with OBJ-format meshes.

Further information:

http://paulbourke.net/dataformats/mtl/

https://en.wikipedia.org/wiki/Material_Template_Library

@param list The Material List to be exported.
@param file_name The file name to write to.
@return Zero on success or non-zero on failure.
*/

int mtrlSaveMTL(const mtrl_list_s *list, const char *file_name);

/**
@brief Replace any DOS/Windows-style backslash path separators.

Windows allows either forward- or back-slash characters to be used in paths,
but UNIX-like systems expect forward-slashes to be used in all cases.

@param material_list The list of materials to be amended.
@return Zero on success, or non-zero on failure.
*/

int mtrlUnixPaths(mtrl_list_s *material_list);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_MATERIAL_H__ */
