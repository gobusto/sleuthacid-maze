/*

FILENAME: mtrl_save.c
AUTHOR/S: Thomas Dennis
CREATION: 3rd March 2012

Copyright (c) 2012-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file mtrl_save.c

@brief Provides utilities relating to mesh material information.

Note that this file does not load textures; it only handles "descriptive" data,
such as the MTLLIB files used by OBJ meshes.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mtrl.h"

/*
[PUBLIC] Save a Material List to an MTL file.
*/

int mtrlSaveMTL(const mtrl_list_s *list, const char *file_name)
{

  FILE   *file = NULL;
  mtrl_s *mat  = NULL;
  long   i     = 0;

  if (!list || !file_name) { return -1; }

  file = fopen (file_name, "wb");

  if (!file) { return 1; }

  for (i = 0; i < list->count; i++)
  {

    mat = list->mtrl[i];

    fprintf (file, "newmtl %s\nd %f\n", mat->name, mat->alpha);

    if (mat->diffusemap[0]) { fprintf (file, "map_Kd %s\n", mat->diffusemap); }
    if (mat->alphamap[0])   { fprintf (file, "map_d %s\n",  mat->alphamap  ); }
    if (mat->bumpmap[0])    { fprintf (file, "bump %s\n",   mat->bumpmap   ); }

  }

  fclose (file);

  return 0;

}
