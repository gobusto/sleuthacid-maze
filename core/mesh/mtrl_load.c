/*

FILENAME: mtrl_load.c
AUTHOR/S: Thomas Dennis
CREATION: 25th December 2010

Copyright (c) 2010-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file mtrl_load.c

@brief Provides utilities relating to mesh material information.

Note that this file does not load textures; it only handles "descriptive" data,
such as the MTLLIB files used by OBJ meshes.
*/

#include <stdlib.h>
#include <string.h>
#include "mtrl.h"
#include "util/misc.h"

/**
@brief [INTERNAL] Material file "parse-state" structure.

This is used to maintain the parser state between each call to mtrlReadLine(),
as using static variables would cause problems for multi-threaded applications.
*/

typedef struct
{

  long        count;  /**< @brief Stores the index of the current material. */
  mtrl_list_s *list;  /**< @brief A material list. Possibly a NULL pointer. */

} mtrl_parse_s;

/**
@brief [INTERNAL] Per-line callback function for MTL files.

@todo Don't assume that the text is followed by a space character - it could be
a tab or something instead. Also, it could possibly be followed by several more
whitespace characters, so don't include those when copying the filename string.

@param line The current line of text.
@param data The parse state object being used.
@return Zero on success, or non-zero on error.
*/

static int mtrlReadLine(const char *line, void *data)
{

  mtrl_s *mtrl = NULL;
  mtrl_parse_s *state = (mtrl_parse_s*)data;

  if (!state) { return -1; }

  /* Check for the start of a new Material definition. */

  if (strncmp(line, "newmtl ", 7) == 0) { state->count++; }

  /* On the first pass, no list is allocated, so just return here. */

  if (!state->list) { return 0;                               }
  else              { mtrl = state->list->mtrl[state->count]; }

  /* Get material name. */

  if (strncmp(line, "newmtl ", 7) == 0)
  { strncpy(mtrl->name, &line[7], MTRL_MAXSTRLEN); }

  /* Get diffuse map. */

  else if (strncmp(line, "map_Kd ", 7) == 0)
  { strncpy(mtrl->diffusemap, &line[7], MTRL_MAXSTRLEN); }

  /* Get alpha map. */

  else if (strncmp(line, "map_d ", 6) == 0)
  { strncpy(mtrl->alphamap, &line[6], MTRL_MAXSTRLEN); }

  /* Get bump map. Some implementations use "map_bump" instead of "bump". */

  else if (strncmp(line, "bump ", 5) == 0)
  { strncpy(mtrl->bumpmap, &line[5], MTRL_MAXSTRLEN); }
  else if (strncmp(line, "map_bump ", 9) == 0)
  { strncpy(mtrl->bumpmap, &line[9], MTRL_MAXSTRLEN); }

  /* Get alpha value. Some implementations use "Tr" instead of "d". */

  else if (strncmp(line, "d ",  2) == 0) { mtrl->alpha = atof(&line[2]); }
  else if (strncmp(line, "Tr ", 3) == 0) { mtrl->alpha = atof(&line[3]); }

  return 0;

}

/*
[PUBLIC] Load a Wavefront MTL file.
*/

mtrl_list_s *mtrlLoadText(const char *text)
{

  mtrl_parse_s state;

  /* First, determine how many materials to allocate memory for. */

  memset(&state, 0, sizeof(mtrl_parse_s));
  txtEachLine(text, "#", mtrlReadLine, &state);

  /* Next, create the materials list and populate it. */

  state.list = mtrlCreateList(state.count);
  state.count = -1;
  txtEachLine(text, "#", mtrlReadLine, &state);

  mtrlUnixPaths(state.list);

  return state.list;

}

/*
[PUBLIC] about
*/

mtrl_list_s *mtrlLoadFile(const char *file_name)
{

  mtrl_list_s *list = NULL;
  char        *text = NULL;

  text = (char*)txtLoadFile(file_name, NULL);
  list = mtrlLoadText(text);
  if (text) { free(text); }

  return list;

}
