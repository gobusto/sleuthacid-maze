/*

FILENAME: mesh_save.c
AUTHOR/S: Thomas Dennis
CREATION: 17th March 2012.

Copyright (c) 2012-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file mesh_save.c

@brief Provides utilities for exporting meshes to a variety of 3D file formats.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mesh.h"

/**
@brief [INTERNAL] Write a little-endian UINT32 value to a file.

@param out The file to write to.
@param value The value to write.
@return Zero on success, or non-zero on failure.
*/

static int morphPutUI32_LE(FILE *out, unsigned long value)
{

  if (!out) { return -1; }

  fputc((value >> 0 ) % 256, out);
  fputc((value >> 8 ) % 256, out);
  fputc((value >> 16) % 256, out);
  fputc((value >> 24) % 256, out);

  return 0;

}

/*
[PUBLIC] Export a mesh in STL format.
*/

int morphSaveSTL(const mesh_s *mesh, long frame, const char *file_name)
{

  FILE *out = NULL;
  mesh_group_s *s = NULL;
  long g, t, i;

  if      (!mesh || !file_name                   ) { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Open the file and output a "header" of 80 zero-bytes. */

  out = fopen(file_name, "wb");
  if (!out) { return -3; }

  for (i = 0; i < 80; i++) { fputc(0, out); }

  /* Get the TOTAL number of triangles in the mesh and write it to the file. */

  for (t = 0, g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g]) { t += mesh->group[g]->num_tris; }
  }

  morphPutUI32_LE(out, t);

  /* Output each triangle. */

  for (g = 0; g < mesh->num_groups; g++)
  {

    s = mesh->group[g];
    if (!s) { continue; }

    for (t = 0; t < s->num_tris; t++)
    {

      /* Face normal. Set to [0, 0, 0] so that it's calculated when loaded. */

      for (i = 0; i < 3 * 4; i++) { fputc(0, out); }

      /* Triangle vertex positions. Notice that these are in reverse order. */

      for (i = 0; i < 3; i++)
      { fwrite(&mesh->vertex[frame][s->tri[t]->v_id[2-i] * 3], 4, 3, out); }

      /* Flags. These will generally always be zero. */

      fputc(0, out);
      fputc(0, out);

    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}

/*
[PUBLIC] Export a Morph Mesh as an OBJ.
*/

int morphSaveOBJ(const mesh_s *mesh, long frame, const char *obj_file,
                                                        const char *mtl_file)
{

  FILE          *out = NULL;
  mesh_group_s *grp = NULL;
  long g, t, i;

  /* Check parameters. */

  if      (!mesh || !obj_file)                     { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Open the file. */

  out = fopen(obj_file, "wb");
  if (!out) { return -3; }

  /* If an MTLLIB has been specified, create the MTL file and reference it. */

  if (mtrlSaveMTL(mesh->mtrl, mtl_file) == 0) { fprintf(out, "mtllib %s\n", mtl_file); }

  /* Write vertices. */

  for (i = 0; i < mesh->num_verts; i++)
  {
    fprintf(out, "v %f %f %f\n", mesh->vertex[frame][(i*3)+0],
                                 mesh->vertex[frame][(i*3)+1],
                                 mesh->vertex[frame][(i*3)+2]);
  }

  /* Write normals. */

  for (i = 0; i < mesh->num_norms; i++)
  {
    fprintf(out, "vn %f %f %f\n", mesh->normal[frame][(i*3)+0],
                                  mesh->normal[frame][(i*3)+1],
                                  mesh->normal[frame][(i*3)+2]);
  }

  /* Write texcoords. */

  for (i = 0; i < mesh->num_coords; i++)
  {
    fprintf(out, "vt %f %f\n", mesh->texcoord[(i*2)+0], mesh->texcoord[(i*2)+1]);
  }

  /* Write groups. */

  for (g = 0; g < mesh->num_groups; g++ )
  {

    grp = mesh->group[g];
    if (!grp) { continue; }

    /* Write group and material data. */

    fprintf(out, "g %s\n", grp->name ? grp->name : "");

    if (mesh->mtrl)
    {
      if (grp->mat_id >= 0 && grp->mat_id < mesh->mtrl->count)
      { fprintf(out, "usemtl %s\n", mesh->mtrl->mtrl[grp->mat_id]->name); }
      else
      { fprintf(out, "usemtl notexture\n"); }
    }

    /* Write faces. */

    for (t = 0; t < grp->num_tris; t++)
    {

      fputc('f', out);

      for (i = 2; i >= 0; i--)
      {

        fprintf(out, " %ld", grp->tri[t]->v_id[i] + 1);

        if (mesh->texcoord) { fprintf(out, "/%ld", grp->tri[t]->t_id[i] + 1); }

        if (mesh->normal)
        {
          if (!mesh->texcoord) { fputc('/', out); }
          fprintf(out, "/%ld", grp->tri[t]->n_id[i] + 1);
        }

      }

      fputc('\n', out);

    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}

/*
[PUBLIC] Export a mesh as a Princeton OFF file.

REFERENCE:

http://shape.cs.princeton.edu/benchmark/documentation/off_format.html
*/

int morphSaveOFF(const mesh_s *mesh, long frame, const char *file_name)
{

  FILE *out = NULL;
  long g, i;

  if      (!mesh || !file_name                   ) { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Get the TOTAL number of triangles in the mesh. */

  for (i = 0, g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g]) { i += mesh->group[g]->num_tris; }
  }

  /* Open the file and output a file header. */

  out = fopen(file_name, "wb");
  if (!out) { return -3; }
  fprintf(out, "OFF %ld %ld 0\n", mesh->num_verts, i);

  /* Output each vertex. */

  for (i = 0; i < mesh->num_verts * 3; i++)
  { fprintf(out, "%f%c", mesh->vertex[frame][i], i % 3 == 2 ? '\n' : ' '); }

  /* Output each triangle. */

  for (g = 0; g < mesh->num_groups; g++)
  {

    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    for (i = 0; i < group->num_tris; i++)
    {
      fprintf(out, "3 %ld %ld %ld\n", group->tri[i]->v_id[2],
                                      group->tri[i]->v_id[1],
                                      group->tri[i]->v_id[0]);
    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}

/*
[PUBLIC] Export a mesh as a Stanford PLY file.

REFERENCE:

http://paulbourke.net/dataformats/ply/
*/

int morphSavePLY(const mesh_s *mesh, long frame, const char *file_name)
{

  FILE *out = NULL;
  long g, i;

  if      (!mesh || !file_name                   ) { return -1; }
  else if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  /* Get the TOTAL number of triangles in the mesh. */

  for (i = 0, g = 0; g < mesh->num_groups; g++)
  {
    if (mesh->group[g]) { i += mesh->group[g]->num_tris; }
  }

  /* Open the file and output a file header. */

  out = fopen(file_name, "wb");
  if (!out) { return -3; }
  fprintf(out, "ply\nformat ascii 1.0\n");

  fprintf(out, "element vertex %ld\n", mesh->num_verts);
  fprintf(out, "property float x\nproperty float y\nproperty float z\n");

  fprintf(out, "element face %ld\n", i);
  fprintf(out, "property list uint uint vertex_index\nend_header\n");

  /* Output each vertex. */

  for (i = 0; i < mesh->num_verts * 3; i++)
  { fprintf(out, "%f%c", mesh->vertex[frame][i], i % 3 == 2 ? '\n' : ' '); }

  /* Output each triangle. */

  for (g = 0; g < mesh->num_groups; g++)
  {

    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    for (i = 0; i < group->num_tris; i++)
    {
      fprintf(out, "3 %ld %ld %ld\n", group->tri[i]->v_id[2],
                                      group->tri[i]->v_id[1],
                                      group->tri[i]->v_id[0]);
    }

  }

  /* Close the file and report success. */

  fclose(out);

  return 0;

}
