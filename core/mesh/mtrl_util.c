/*

FILENAME: mtrl_util.c
AUTHOR/S: Thomas Dennis
CREATION: 25th December 2010

Copyright (c) 2010-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**

\file mtrl_util.c

\brief Provides utilities relating to mesh material information.

Note that this file does not load textures; it only handles "descriptive" data,
such as the MTLLIB files used by OBJ meshes.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mtrl.h"

/*

[PUBLIC] Create a material list.

*/

mtrl_list_s *mtrlCreateList (long size)
{

  mtrl_list_s *list = NULL;
  long        i     = 0;
  int         fail  = 0;

  if (size < 1) { return NULL; }

  /* Create the main list structure. */

  list = (mtrl_list_s*)malloc(sizeof(mtrl_list_s));
  if (!list) { return NULL; }
  memset(list, 0, sizeof(mtrl_list_s));

  list->count = size;

  list->mtrl  = (mtrl_s**)malloc(list->count * sizeof(mtrl_s*));
  if (!list->mtrl) { return mtrlFreeList(list); }

  /* Create the individual list items. */

  for (fail = 0, i = 0; i < list->count; i++)
  {

    list->mtrl[i] = (mtrl_s*)malloc(sizeof(mtrl_s));

    if (list->mtrl[i])
    {

      memset(list->mtrl[i], 0, sizeof(mtrl_s));

      list->mtrl[i]->alpha = 1.0;

    } else { fail = 1; }

  }

  /* Return the result. */

  return fail ? mtrlFreeList(list) : list;

}

/*

[PUBLIC] Free a material list.

*/

mtrl_list_s *mtrlFreeList(mtrl_list_s *list)
{

  long i = 0;

  if (!list) { return NULL; }

  if (list->mtrl)
  {

    for (i = 0; i < list->count; i++)
    {

      if (list->mtrl[i]) { free (list->mtrl[i]); }

    }

    free (list->mtrl);

  }

  free (list);

  return NULL;

}

/*

[PUBLIC] Find a material in a list.

*/

long mtrlFind (const mtrl_list_s *list, const char *name)
{

  long i = 0;

  if (!name || !list) { return -1; }

  for (i = 0; i < list->count; i++)
  {

    if (strcmp (name, list->mtrl[i]->name) == 0) { return i; }

  }

  return -1;

}

/*
[PUBLIC] Replace any DOS/Windows-style backslash path separators.
*/

int mtrlUnixPaths(mtrl_list_s *material_list)
{

  long m;

  if (!material_list) { return -1; }

  for (m = 0; m < material_list->count; m++)
  {

    mtrl_s *mtrl = material_list->mtrl[m];
    size_t i, len;

    for (i = 0, len = strlen(mtrl->diffusemap); i < len; i++)
    { if (mtrl->diffusemap[i] == '\\') { mtrl->diffusemap[i] = '/'; } }

    for (i = 0, len = strlen(mtrl->bumpmap); i < len; i++)
    { if (mtrl->bumpmap[i] == '\\') { mtrl->bumpmap[i] = '/'; } }

    for (i = 0, len = strlen(mtrl->alphamap); i < len; i++)
    { if (mtrl->alphamap[i] == '\\') { mtrl->alphamap[i] = '/'; } }

  }

  return 0;

}
