/*

FILENAME: mesh_load.c
AUTHOR/S: Thomas Dennis
CREATION: Sometime 2008

Copyright (c) 2008-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file mesh_load.c

@brief Provides utilities for importing meshes in a variety of 3D file formats.
*/

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "mesh.h"
#include "util/misc.h"

/* MilkShape MS3D constants. */

#define MS3D_SIZE_VERT  15  /**< @brief MS3D: Size of a single vertex.      */
#define MS3D_SIZE_FACE  70  /**< @brief MS3D: Size of a single face.        */
#define MS3D_SIZE_GROUP 36  /**< @brief MS3D: BASIC size of a single group. */
#define MS3D_SIZE_MTRL  361 /**< @brief MS3D: Size of a single material.    */
#define MS3D_SIZE_BONE  93  /**< @brief MS3D: BASIC size of a single joint. */

#define MS3D_SIZE_ANIM_ROTATION 16  /**< @brief MS3D: Rotation frame size. */
#define MS3D_SIZE_ANIM_POSITION 16  /**< @brief MS3D: Position frame size. */

/* Quake 3 MD3 constants. */

#define MD3_XYZ_SCALE (1.0/64.0)  /**< @brief MD3: Vertex XYZ scaling factor. */

#define MD3_SIZE_HEAD         108 /**< @brief MD3: File HEADER size in bytes. */
#define MD3_HEAD_IDP3         0   /**< @brief MD3: Offset to magic number.    */
#define MD3_HEAD_VERSION      4   /**< @brief MD3: Offset to format version.  */
#define MD3_HEAD_NAME         8   /**< @brief MD3: Offset to model name.      */
#define MD3_HEAD_FLAGS        72  /**< @brief MD3: Offset to flags. Ignored.  */
#define MD3_HEAD_NUM_FRAMES   76  /**< @brief MD3: Offset to frame count.     */
#define MD3_HEAD_NUM_TAGS     80  /**< @brief MD3: Offset to tag count.       */
#define MD3_HEAD_NUM_SURFACES 84  /**< @brief MD3: Offset to surface count.   */
#define MD3_HEAD_NUM_UNUSED   88  /**< @brief MD3: Legacy skin count. Unused. */
#define MD3_HEAD_POS_FRAMES   92  /**< @brief MD3: Offset to frame offset.    */
#define MD3_HEAD_POS_TAGS     96  /**< @brief MD3: Offset to tag offset.      */
#define MD3_HEAD_POS_SURFACES 100 /**< @brief MD3: Offset to surface offset.  */
#define MD3_HEAD_POS_END      104 /**< @brief MD3: Offset to end-of-file.     */

#define MD3_SIZE_SURF         108 /**< @brief MD3: Surface HEADER size.       */
#define MD3_SURF_IDP3         0   /**< @brief MD3: Offset to magic number.    */
#define MD3_SURF_NAME         4   /**< @brief MD3: Offset to Surface name.    */
#define MD3_SURF_FLAGS        68  /**< @brief MD3: Offset to flags. Ignored.  */
#define MD3_SURF_NUM_FRAMES   72  /**< @brief MD3: Offset to frame count.     */
#define MD3_SURF_NUM_SKINS    76  /**< @brief MD3: Offset to skin count.      */
#define MD3_SURF_NUM_VERTS    80  /**< @brief MD3: Offset to vertex count.    */
#define MD3_SURF_NUM_FACES    84  /**< @brief MD3: Offset to triangle count.  */
#define MD3_SURF_POS_FACES    88  /**< @brief MD3: Offset to triangle offset. */
#define MD3_SURF_POS_SKINS    92  /**< @brief MD3: Offset to skin offset.     */
#define MD3_SURF_POS_COORDS   96  /**< @brief MD3: Offset to texcoord offset. */
#define MD3_SURF_POS_VERTS    100 /**< @brief MD3: Offset to vertex offset.   */
#define MD3_SURF_POS_END      104 /**< @brief MD3: Offset to surf-end offset. */

#define MD3_SIZE_FRAME        56  /**< @brief MD3: Frame chunk size in bytes. */
#define MD3_FRAME_NAME        40  /**< @brief MD3: Rel. offset to frame name. */

#define MD3_SIZE_TAG          112 /**< @brief MD3: Tag chunk size in bytes.   */
#define MD3_TAG_NAME          0   /**< @brief MD3: Rel. offset to tag name.   */
#define MD3_TAG_ORIGIN        64  /**< @brief MD3: Rel. offset to tag origin. */
#define MD3_TAG_MATRIX        76  /**< @brief MD3: Rel. offset to tag matrix. */

/* Quake 4 MD5 constants. */

#define MD5_VERSION "MD5Version 10" /**< @brief MD5: MD5mesh version string. */

/* LWO 4CCs. Usually given as text, but defined in hex for switch statements. */

#define LWO_VERSION_1 0x4C574F42  /**< @brief LWO: "LWOB" FORM type of v1. */
#define LWO_VERSION_2 0x4C574F32  /**< @brief LWO: "LWO2" FORM type of v2. */

/* Python code: print("0x"+"".join(hex(ord(x)).upper()[2:] for x in "ABCD")) */

#define ID_LWO_MAIN     0x464F524D  /**< @brief LWO: "FORM" Root IFF node. */
#define ID_LWO_VERTICES 0x504E5453  /**< @brief LWO: "PNTS" Vertices data. */
#define ID_LWO_POLYGONS 0x504F4C53  /**< @brief LWO: "POLS" Polygons data. */
#define ID_LWO_VMAP     0x564D4150  /**< @brief LWO: "VMAP" Vertex params. */

#define LWO_POLY_FACE   0x46414345  /**< @brief LWO: "FACE" Face data type. */

#define LWO_VMAP_COORDS 0x54585556  /**< @brief LWO: "TXUV" Texcoords data. */

/* 3DS constants. */

#define ID_3DS_MAIN3DS       0x4D4D /**< @brief 3DS: Main chunk ID.      */
#define ID_3DS_EDIT3DS       0x3D3D /**< @brief 3DS: Mesh chunk ID.      */
#define ID_3DS_KEYF3DS       0xB000 /**< @brief 3DS: Animation chunk ID. */

#define ID_3DS_EDIT_MATERIAL 0xAFFF /**< @brief 3DS: Material list chunk ID. */
#define ID_3DS_EDIT_OBJECT   0x4000 /**< @brief 3DS: Object data chunk ID.   */

#define ID_3DS_MTRL_NAME     0xA000 /**> @brief 3DS: Material name chunk.     */
#define ID_3DS_MTRL_DIFFUSE  0xA200 /**< @brief 3DS: Material diffuse chunk.  */
#define ID_3DS_MTRL_FILENAME 0xA300 /**< @brief 3DS: Material filename chunk. */

#define ID_3DS_OBJECT_MESH   0x4100 /**< @brief 3DS: Trimesh data chunk ID.  */

#define ID_3DS_MESH_VERTICES 0x4110 /**< @brief 3DS: Trimesh vertices chunk.  */
#define ID_3DS_MESH_TRIANGLE 0x4120 /**< @brief 3DS: Trimesh triangles chunk. */

#define ID_3DS_MESH_TEXCOORD 0x4140 /**< @brief 3DS: Trimesh texcoords chunk. */

#define ID_3DS_MESH_MATRIX   0x4160 /**< @brief 3DS: Trimesh matrix chunk.    */

/**
@brief [INTERNAL] Defines a single vertex weight in a skeletally animated mesh.

This is currently only used when loading Doom 3 / Quake 4 *.md5mesh files.
*/

typedef struct
{

  long      joint_id; /**< @brief The index of the tag that the weight uses. */
  float     bias;     /**< @brief Influence of the weight/tag on the vertex. */
  vector3_t xyz;      /**< @brief The position of this vertex weight.        */

} mesh_weight_s;

/* Parse state flags. */

#define MESH_PARSE_FLAG_MD5JOINTS 0x01 /**< @brief MD5: Set if parsing tags. */

/**
@brief [INTERNAL] The structure used for parsing text-based formats.

This is used to keep track of the current "parser state" through multiple calls
to per-line callback functions which would otherwise have to rely on statically
allocated variables. Since it is possible for a multi-threaded program to parse
multiple files at the same time, static variables are not an ideal solution.
*/

typedef struct
{

  unsigned long flags;  /**< @brief State flags OR current 3DS/LWO chunk ID. */

  mesh_s *mesh; /**< @brief The mesh being created/constructed. */

  long num_groups;  /**< @brief Number of groups [in total/read so far].    */
  long num_joints;  /**< @brief Number of joints [in total/read so far].    */
  long num_coords;  /**< @brief Number of texcoords [in total/read so far]. */
  long num_verts;   /**< @brief Number of vertices [in total/read so far].  */
  long num_norms;   /**< @brief Number of normals [in total/read so far].   */
  long num_tris;    /**< @brief Number of triangles [in total/read so far]. */
  long num_mtrls;   /**< @brief Number of materials [in total/read so far]. */

  long mat_id;      /**< @brief The current material index.      */
  long offs_vertex; /**< @brief The current vertex index offset. */

  long next_weight;       /**< @brief MD5mesh only: The current joint index. */
  long num_weights;       /**< @brief MD5mesh only: The number of weights.   */
  mesh_weight_s **weight; /**< @brief MD5mesh only: Array of vertex weights. */

} mesh_parse_s;

/**
@brief [INTERNAL] Generate "smooth" normals for a mesh.

@todo Doesn't (yet) normalise the generated vectors.

@todo Return a value to indicate success/failure?

@todo Perhaps also add another function for flat-face normals?

@param mesh The mesh to generate normals for.
*/

static void morphGenerateNormals(mesh_s *mesh)
{

  mesh_group_s *grp = NULL;

  float vecA[3];
  float vecB[3];

  short fail = 0;

  long f, g, t, i;

  /* Ensure that the mesh exists and does not already have normal data. */

  if (!mesh       ) { return; }
  if (mesh->normal) { return; }

  /* Allocate memory. */

  mesh->normal = (float**)malloc (sizeof(float*) * mesh->num_frames);

  if (!mesh->normal) { return; }

  mesh->num_norms = mesh->num_verts;

  /* Loop through all frames of animation. */

  for (f = 0; f < mesh->num_frames; f++)
  {

    /* Allocate memory. */

    mesh->normal[f] = (float*)malloc (sizeof(float) * mesh->num_norms * 3);

    if (mesh->normal[f])
    {

      memset (mesh->normal[f], 0, sizeof(float) * mesh->num_norms * 3);

      /* Loop through all triangles. */

      for (g = 0; g < mesh->num_groups; g++)
      {

        grp = mesh->group[g];
        if (!grp) { continue; }

        /* Generate cross-products for each triangle in this frame. */

        for (t = 0; t < grp->num_tris; t++)
        {

          for (i = 0; i < 3; i++)
          {

            grp->tri[t]->n_id[i] = grp->tri[t]->v_id[i];

            vecA[i] = mesh->vertex[f][(grp->tri[t]->v_id[1]*3)+i]
                    - mesh->vertex[f][(grp->tri[t]->v_id[0]*3)+i];
            vecB[i] = mesh->vertex[f][(grp->tri[t]->v_id[2]*3)+i]
                    - mesh->vertex[f][(grp->tri[t]->v_id[0]*3)+i];

          }

          for (i = 0; i < 3; i++)
          {

            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+0] -=
                                  vecA[1]*vecB[2] - vecA[2]*vecB[1];
            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+1] -=
                                  vecA[2]*vecB[0] - vecA[0]*vecB[2];
            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+2] -=
                                  vecA[0]*vecB[1] - vecA[1]*vecB[0];

          }

        }

      }

    } else { fail = 1; }

  }

  /* Check failure flag. */

  if (fail)
  {

    /* TODO: Free the whole normals array again... */

  }

}

/**
@brief [INTERNAL] Allocate a new, blank morph-mesh structure.

This function is mostly to avoid copy-pasting the same allocation code into all
import functions. Note that this function always allocates ZERO materials, so
functions must allocate a material list by calling mtrlCreateList() themselves.
The same applies to Tags, because not all file formats will need to use them.

@param frm The number of animation frames to allocate memory for.
@param vrt The number of vertices to allocate memory for.
@param nrm The number of normals to allocate memory for.
@param tex The number of texture coordinates to allocate memory for.
@param grp The number of groups to allocate memory for.
@return A new morph-mesh structure on success or NULL on failure.
*/

static mesh_s *morphCreate(long frm, long vrt, long nrm, long tex, long grp)
{

  mesh_s *mesh = NULL;  /* The mesh to be returned. */
  short         fail = 0;     /* Allocation failure flag. */
  long          i    = 0;     /* Common FOR loop counter. */

  /* Check parameters. */

  if (frm < 1 || vrt < 3 || nrm < 0 || tex < 0 || grp < 1) { return NULL; }

  /* Allocate a new mesh structure. */

  mesh = (mesh_s*)malloc(sizeof(mesh_s));
  if (!mesh) { return NULL; }
  memset(mesh, 0, sizeof(mesh_s));

  /* Initialise counters. */

  mesh->num_frames = frm;
  mesh->num_verts  = vrt;
  mesh->num_norms  = nrm;
  mesh->num_coords = tex;
  mesh->num_groups = grp;

  /* Initialise Failure flag. */

  fail = 0;

  /* Allocate memory for frames. */

  if (mesh->num_frames > 0)
  {

    mesh->frame = (mesh_frame_s**)malloc (sizeof(mesh_frame_s*) * frm);

    if (mesh->frame)
    {

      for (i = 0; i < frm; i++)
      {

        mesh->frame[i] = (mesh_frame_s*)malloc (sizeof(mesh_frame_s));

        if (mesh->frame[i]) { memset (mesh->frame[i], 0, sizeof(mesh_frame_s)); }
        else { fail = 1; }

      }

    } else { fail = 1; }

  }

  /* Allocate memory for vertices. */

  if (mesh->num_verts > 0)
  {

    mesh->vertex = (float**)malloc (sizeof(float*) * frm);

    if (mesh->vertex)
    {

      for (i = 0; i < frm; i++)
      {

        mesh->vertex[i] = (float*)malloc (sizeof(float) * vrt * 3);

        if (mesh->vertex[i]) { memset (mesh->vertex[i], 0, sizeof(float) * vrt * 3); }
        else { fail = 1; }

      }

    } else { fail = 1; }

  }

  /* Allocate memory for normals. */

  if (mesh->num_norms > 0)
  {

    mesh->normal = (float**)malloc (sizeof(float*) * frm);

    if (mesh->normal)
    {

      for (i = 0; i < mesh->num_frames; i++)
      {

        mesh->normal[i] = (float*)malloc (sizeof(float) * nrm * 3);

        if (mesh->normal[i]) { memset (mesh->normal[i], 0, sizeof(float) * nrm * 3); }
        else { fail = 1; }

      }

    } else { fail = 1; }

  }

  /* Allocate memory for texcoords. */

  if (mesh->num_coords > 0)
  {

    mesh->texcoord = (float*)malloc (sizeof(float) * tex * 2);

    if (mesh->texcoord) { memset (mesh->texcoord, 0, sizeof(float) * tex * 2); }
    else { fail = 1; }

  }

  /* Allocate memory for group pointer array (NOT the actual groups!). */

  if (mesh->num_groups > 0)
  {

    mesh->group = (mesh_group_s**)malloc (sizeof(mesh_group_s*) * grp);

    if (mesh->group) { memset (mesh->group, 0, sizeof(mesh_group_s*) * grp); }
    else             { fail = 1; }

  }

  /* If any part of the initialisation has failed, destroy the faulty mesh. */

  if (fail)
  {

    morphFree (mesh);

    return NULL;

  }

  /* Otherwise, return the result. */

  return mesh;

}

/**
@brief [INTERNAL] Allocate a new, blank triangle group.

This function is mostly to avoid copy-pasting the same allocation code into all
import functions.

@param name The name of thr triangle group.
@param mat_id The material ID. Can be negative to indicate no material.
@param num_tris The number of triangles to allocate memory for.
@return A new Morph Group structure on success or NULL on failure.
*/

static mesh_group_s *morphNewGroup(const char *name, long mat_id, long num_tris)
{

  mesh_group_s *grp = NULL;  /* The group to return.     */
  short         fail = 0;     /* Allocation failure flag. */
  long          i    = 0;     /* Common FOR loop counter. */

  /* Check parameters. */

  if (num_tris < 1) { return NULL; }

  /* Allocate a new group structure. */

  grp = (mesh_group_s*)malloc (sizeof(mesh_group_s));

  if (!grp) { return NULL; }

  memset (grp, 0, sizeof(mesh_group_s));

  /* Initialise it. */

  if (name)
  {
    grp->name = (char*)malloc(strlen(name) + 1);
    if (grp->name) { strcpy (grp->name, name); }
  }

  grp->mat_id   = mat_id;
  grp->num_tris = num_tris;

  /* Allocate memory for triangles. */

  grp->tri = (mesh_tri_s**)malloc (sizeof(mesh_tri_s*) * num_tris);

  if (grp->tri)
  {

    for (i = 0; i < num_tris; i++)
    {

      grp->tri[i] = (mesh_tri_s*)malloc (sizeof(mesh_tri_s));

      if (grp->tri[i]) { memset (grp->tri[i], 0, sizeof(mesh_tri_s)); }
      else             { fail = 1;                                     }

    }

  } else { fail = 1; }

  /* If anything went wrong, free the group and return NULL. */

  if (fail)
  {

    /* TODO */

  }

  /* Otherwise, return the result. */

  return grp;

}

/**
@brief [INTERNAL] Load a MilkShape3D-format mesh.

@todo Could use some tidying up.

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadMS3D(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;

  long local_faces, num_tris,  num_groups,  num_mtrls,  num_verts, num_joints;
  long total_faces, offs_tris, offs_groups, offs_mtrls, offs, offs_joints, t, a, i, j;

  /* Ensure that the buffer is a valid MS3D file. */

  if (length < 36 || !data) { return NULL; }

  i = binGetUI32_LE(&data[10]);

  if (memcmp(data, "MS3D000000", 10) || i < 3 || i > 4) { return NULL; }

  /* Get the number of vertices. */

  offs = 14;

  num_verts = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs += MS3D_SIZE_VERT * num_verts;

  /* Get the number of triangles and their offset. */

  num_tris = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_tris = offs;
  offs += MS3D_SIZE_FACE * num_tris;

  /* Get the number of groups and their offset. NOTE: Group size varies. */

  num_groups = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_groups = offs;

  for (i = 0; i < num_groups; i++)
  { offs += MS3D_SIZE_GROUP + (binGetUI16_LE(&data[offs+33]) * 2); }

  /* Get the number of materials and their offset. */

  num_mtrls = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_mtrls = offs;
  offs += MS3D_SIZE_MTRL * num_mtrls;

  /* Skip some skeletal animation data. */

  offs += 12;

  /* Get the number of joints and their offset. */

  num_joints = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_joints = offs;

  /* Allocate a mesh structure (Normals and Coords are stored in triangles). */

  mesh = morphCreate(1, num_verts, num_tris * 3, num_tris * 3, num_groups);

  if (!mesh) { return NULL; }

  mesh->mtrl = mtrlCreateList(num_mtrls);

  /* Create a vertex -> joint association array. */

  mesh->vert_tag = (long*)malloc(sizeof(long) * num_verts);

  /* Load vertices. */

  for (offs = 16, i = 0; i < num_verts; i++, offs += MS3D_SIZE_VERT)
  {
    memcpy(&mesh->vertex[0][i*3], &data[offs + 1], sizeof(float) * 3);
    if (mesh->vert_tag) { mesh->vert_tag[i] = (signed char)data[offs + 13]; }
  }

  /* Load groups. */

  offs        = offs_groups;
  total_faces = 0;

  for (i = 0; i < num_groups; i++)
  {

    /* Get number of triangles (It's after the flags byte and name string). */

    local_faces = binGetUI16_LE(&data[offs+33]);

    /* Create group. */

    mesh->group[i] = morphNewGroup((char*)&data[offs+1],
                                  data[offs+MS3D_SIZE_GROUP+(local_faces*2)-1],
                                  local_faces);

    if (mesh->group[i])
    {

      /* Loop through every face within this group. */

      for (t = 0; t < local_faces; t++)
      {

        /* Get the offset of the triangle data within the file. */

        j = offs_tris + (binGetUI16_LE(&data[offs+35+(t*2)]) * MS3D_SIZE_FACE);

        /* Copy the vertex, texcoord, and normal data. */

        for (a = 0; a < 3; a++)
        {

          mesh->group[i]->tri[t]->v_id[a] = binGetUI16_LE(&data[j+2+(a*2)]);
          mesh->group[i]->tri[t]->n_id[a] = ((total_faces+t) * 3) + a;
          mesh->group[i]->tri[t]->t_id[a] = ((total_faces+t) * 3) + a;

          memcpy(&mesh->normal[0][mesh->group[i]->tri[t]->n_id[a]*3],
                &data[j+8+(a*4*3)], 4*3);

          memcpy(&mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+0],
                &data[j+44+(a*4)], 4);

          memcpy(&mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1],
                &data[j+56+(a*4)], 4);

          mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1] = 1.0f -
          mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1];

        }

      }

    } else { /* TODO: Flag this as an error. */ }

    /* Move on to the next group and increment the total_faces counter. */

    offs += MS3D_SIZE_GROUP + (local_faces*2);

    total_faces += local_faces;

  }

  /* Load mesh material data. */

  offs = offs_mtrls;

  if (mesh->mtrl)
  {

    for (i = 0; i < num_mtrls; i++)
    {

      strncpy(mesh->mtrl->mtrl[i]->name,       (char*)&data[offs    ], MTRL_MAXSTRLEN);
      strncpy(mesh->mtrl->mtrl[i]->diffusemap, (char*)&data[offs+105], MTRL_MAXSTRLEN);
      strncpy(mesh->mtrl->mtrl[i]->alphamap,   (char*)&data[offs+233], MTRL_MAXSTRLEN);
      memcpy(&mesh->mtrl->mtrl[i]->alpha,             &data[offs+100], 4             );

      offs += MS3D_SIZE_MTRL;

    }

  }

  /* Load joints. NOTE: Currently assumes "parents" are before child joints. */

  mesh->tag_list = meshTagSetCreate(num_joints);

  if (mesh->tag_list)
  {

    for (offs = offs_joints, i = 0; i < num_joints; i++, offs += MS3D_SIZE_BONE)
    {

      unsigned short num_rotation_animation_frames = 0;
      unsigned short num_position_animation_frames = 0;

      char parent[33];

      vector3_t xyz;

      /* Load the joint name and position data. */

      memcpy(mesh->tag_list->tag[i]->name, &data[offs + 1], 32);
      memcpy(mesh->tag_list->tag[i]->origin, &data[offs + 77], sizeof(float) * 3);

      /* Create a quaternion from the euler angles provided. */

      memcpy(xyz, &data[offs + 65], sizeof(float) * 3);
      quatFromEuler(mesh->tag_list->tag[i]->quaternion, xyz[0], xyz[1], xyz[2]);

      /* Find the "parent" of this joint, of one exists. */

      memset(parent, 0, 33);
      memcpy(parent, &data[offs + 33], 32);

      for (j = 0; j < i; j++)
      {
        if (!strcmp(parent, mesh->tag_list->tag[j]->name)) { mesh->tag_list->tag[i]->parent = j; }
      }

      /* Offset this joint to the "final" position. */

      j = mesh->tag_list->tag[i]->parent;

      if (j >= 0)
      {

        quaternion_t wxyz;
        quaternion_t temp;

        /* Get the final tag orientation. */

        wxyz[QUAT_W] = mesh->tag_list->tag[i]->quaternion[QUAT_W];
        wxyz[QUAT_X] = mesh->tag_list->tag[i]->quaternion[QUAT_X];
        wxyz[QUAT_Y] = mesh->tag_list->tag[i]->quaternion[QUAT_Y];
        wxyz[QUAT_Z] = mesh->tag_list->tag[i]->quaternion[QUAT_Z];

        quatMultiply(mesh->tag_list->tag[i]->quaternion, mesh->tag_list->tag[j]->quaternion, wxyz);

        /* Get the final tag position. */

        wxyz[QUAT_W] = 0;
        wxyz[QUAT_X] = mesh->tag_list->tag[i]->origin[0];
        wxyz[QUAT_Y] = mesh->tag_list->tag[i]->origin[1];
        wxyz[QUAT_Z] = mesh->tag_list->tag[i]->origin[2];

        quatRotate(temp, wxyz, mesh->tag_list->tag[j]->quaternion);

        mesh->tag_list->tag[i]->origin[0] = temp[QUAT_X] + mesh->tag_list->tag[j]->origin[0];
        mesh->tag_list->tag[i]->origin[1] = temp[QUAT_Y] + mesh->tag_list->tag[j]->origin[1];
        mesh->tag_list->tag[i]->origin[2] = temp[QUAT_Z] + mesh->tag_list->tag[j]->origin[2];

      }

      /* Skip past the joint animation key-frame data. */

      num_rotation_animation_frames = binGetUI16_LE(&data[offs + 89]);
      num_position_animation_frames = binGetUI16_LE(&data[offs + 91]);

      offs += num_rotation_animation_frames * MS3D_SIZE_ANIM_ROTATION;
      offs += num_position_animation_frames * MS3D_SIZE_ANIM_POSITION;

    }

  }

  /* Return the result. */

  morphReverseFaces(mesh);
  morphInvertNormals(mesh);

  return mesh;

}

/**
@brief [INTERNAL] Handle an LWO chunk.

Work-in-progress.

@param state writeme.
@param length The total size of the data[] array.
@param data An array of raw byte values.
@return Zero on success, or non-zero on failure.
*/

static int morphLoadLWO_chunk(mesh_parse_s *state, unsigned long length, const unsigned char *data)
{

  unsigned long chunk_size, offset;
  unsigned long num_items, i;

  if (!state || length < 8 || !data) { return 666; }

  /* The first 4 bytes of the main FORM chunk identify the IFF data type. */

  offset = 0;

  if (state->flags == ID_LWO_MAIN)
  {

    if      (binGetUI32_BE(&data[offset]) == LWO_VERSION_2) { }
/*    else if (binGetUI32_BE(&data[offset]) == LWO_VERSION_1) { } */
    else                                                    { return 666; }

    offset = 4;

  }

  /* Loop through each sub-chunk within the current chunk. */

  for (; offset <= length - 8; offset += chunk_size + (chunk_size % 2))
  {

    chunk_size = 8 + binGetUI32_BE(&data[offset + 4]);
    if (chunk_size < 8 || chunk_size > length-offset) { return 666; }

    switch (binGetUI32_BE(&data[offset]))
    {

      case ID_LWO_VERTICES:
        if (state->flags != ID_LWO_MAIN) { return 666; }
        fprintf(stderr, "[LWO] %lu vertices found.\n", (chunk_size-8)/12);
      break;

      case ID_LWO_POLYGONS:
        if (state->flags != ID_LWO_MAIN) { return 666; }

        if (binGetUI32_BE(&data[offset+8]) == LWO_POLY_FACE)
        {

          /* NOTE: The highest 6 bits are flags, and should be ignored! */

          for (i = 12; i <= chunk_size - 8; i += 2 + (num_items*2))
          {
            num_items = binGetUI16_BE(&data[offset+i]) & 1023;
            state->num_tris += num_items - 2; /* 3 verts = 1 tri, 4 = 2 tris. */
          }

          fprintf(stderr, "[LWO] %lu triangles found.\n", state->num_tris);

        }

      break;

      default:
/*
        fprintf(stderr, "[DEBUG] Unknown LWO Chunk ID: %c%c%c%c\n",
            (int)(binGetUI32_BE(&data[offset]) >> 24) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >> 16) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >>  8) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >>  0) & 0xFF);
*/
      break;

    }

  }

  /* No problems found; report success. */

  return 0;

}

/**
@brief [INTERNAL] Load a Lightwave LWO-format mesh.

REFERENCE:

http://en.wikipedia.org/wiki/Interchange_File_Format

http://paulbourke.net/dataformats/lightwave/

http://web.archive.org/web/20010423140254/http://members.home.net/erniew2/lwsdk/docs/filefmts/lwo2.html

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadLWO(long length, const unsigned char *data)
{

  mesh_parse_s state;
  unsigned long data_size = 0;

  if      (length <= 8 || !data)               { return NULL; }
  else if (binGetUI32_BE(data) != ID_LWO_MAIN) { return NULL; }

  data_size = 8 + binGetUI32_BE(&data[4]);
  if (data_size > (unsigned long)length) { return NULL; }

  memset(&state, 0, sizeof(mesh_parse_s));
  state.flags = ID_LWO_MAIN;

  if (morphLoadLWO_chunk(&state, data_size-8, &data[8]) == 0)
  { morphGenerateNormals(state.mesh); }
  else
  { state.mesh = morphFree(state.mesh); }

  return state.mesh;

}

/**
@brief [INTERNAL] Handle a 3DS chunk.

Work-in-progress.

@param state writeme.
@param length The total size of the data[] array.
@param data An array of raw byte values.
@return Zero on success, or non-zero on failure.
*/

static int morphLoad3DS_chunk(mesh_parse_s *state, unsigned long length, const unsigned char *data)
{

  unsigned long chunk_size, offset;
  int number_of_passes, pass;

  unsigned short num_items;

  if (!state || length < 6 || !data) { return 666; }

  /* Some chunks need two passes: One to count things, another to load them. */

  if (state->flags == ID_3DS_EDIT3DS) { number_of_passes = 2; }
  else                                { number_of_passes = 1; }

  for (pass = 0; pass < number_of_passes; pass++)
  {

    /* For the main Edit3DS chunk, reset the various counters on each pass. */

    if (state->flags == ID_3DS_EDIT3DS)
    {
      state->num_verts  = 0;
      state->num_coords = 0;
      state->num_groups = 0;
      state->num_mtrls  = 0;
    }

    /* Object chunks begin with a NULL-terminated name string. */

    if (state->flags == ID_3DS_EDIT_OBJECT)
    {

      for (offset = 0; data[offset]; offset++) { /* ... */ }
      offset++; /** <-- NULL terminator byte. */

    } else { offset = 0; }

    /* Loop through each sub-chunk within the current chunk. */

    for (; offset <= length - 6; offset += chunk_size)
    {

      chunk_size = binGetUI32_LE(&data[offset + 2]);
      if (chunk_size < 6 || chunk_size > length-offset) { return 666; }

      switch (binGetUI16_LE(&data[offset]))
      {

        /* The keyframe chunk contains animation data. */

        case ID_3DS_KEYF3DS:
          if (state->flags != ID_3DS_MAIN3DS) { return 666; }
        break;  /* Animation key-frame data is currently ignored. */

        /* The Edit3DS chunk contains the actual scene data, such as meshes. */

        case ID_3DS_EDIT3DS:
          if (state->flags != ID_3DS_MAIN3DS) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
        return morphLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]);

        /* The Material chunk contains a set of material definition chunks. */

        case ID_3DS_EDIT_MATERIAL:
          if (state->flags != ID_3DS_EDIT3DS) { return 666; }
          state->flags = ID_3DS_EDIT_MATERIAL;
          if (morphLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
          state->num_mtrls++;
        break;

        /* The Material name chunk contains just the name, and nothing else. */

        case ID_3DS_MTRL_NAME:
          if (state->flags != ID_3DS_EDIT_MATERIAL) { return 666; }
          if (state->mesh) { strncpy(state->mesh->mtrl->mtrl[state->num_mtrls]->name, (const char*)&data[offset+6], MTRL_MAXSTRLEN); }
        break;

        /* The Diffuse chunk contains various diffuse material parameters. */

        case ID_3DS_MTRL_DIFFUSE:
          if (state->flags != ID_3DS_EDIT_MATERIAL) { return 666; }
          state->flags = ID_3DS_MTRL_DIFFUSE;
          if (morphLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT_MATERIAL;
        break;

        /* The filename chunk defines which texture file to use. */

        case ID_3DS_MTRL_FILENAME:
          if (state->flags != ID_3DS_MTRL_DIFFUSE) { return 666; }
          if (state->mesh) { strncpy(state->mesh->mtrl->mtrl[state->num_mtrls]->diffusemap, (const char*)&data[offset+6], MTRL_MAXSTRLEN); }
        break;

        /* Object chunks may contain lights, cameras, or meshes. */

        case ID_3DS_EDIT_OBJECT:
          if (state->flags != ID_3DS_EDIT3DS) { return 666; }
          state->flags = ID_3DS_EDIT_OBJECT;
          if (morphLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT3DS;
        break;

        /* Trimesh data defines an actual polygonal mesh object. */

        case ID_3DS_OBJECT_MESH:
          state->offs_vertex = state->num_verts;
          state->mat_id = -1;
          if (state->flags != ID_3DS_EDIT_OBJECT) { return 666; }
          state->flags = ID_3DS_OBJECT_MESH;
          if (morphLoad3DS_chunk(state, chunk_size - 6, &data[offset + 6]) != 0) { return 666; }
          state->flags = ID_3DS_EDIT_OBJECT;
          if (state->mesh && state->num_groups > 0) { state->mesh->group[state->num_groups-1]->mat_id = state->mat_id; }
        break;

        /* writeme */

        case ID_3DS_MESH_MATRIX:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          if (state->mesh) { fprintf(stderr, "[FIXME] 3DS reference matrix ignored.\n"); }
        break;

        /* writeme */

        case ID_3DS_MESH_VERTICES:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          num_items = binGetUI16_LE(&data[offset+6]);
          if (state->mesh) { memcpy(&state->mesh->vertex[0][state->num_verts*3], &data[offset+8], sizeof(float) * num_items*3); }
          state->num_verts += num_items;
        break;

        /* writeme */

        case ID_3DS_MESH_TEXCOORD:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }
          num_items = binGetUI16_LE(&data[offset+6]);
          if (state->mesh) { memcpy(&state->mesh->texcoord[state->num_coords*2], &data[offset+8], sizeof(float) * num_items*2); }
          state->num_coords += num_items;
        break;

        /* writeme */

        case ID_3DS_MESH_TRIANGLE:
          if (state->flags != ID_3DS_OBJECT_MESH) { return 666; }

          if (state->mesh)
          {

            long i = 0;

            /* TODO: Use the actual group name here... */
            state->mesh->group[state->num_groups] = morphNewGroup(NULL, -1, binGetUI16_LE(&data[offset+6]));
            if (!state->mesh->group[state->num_groups]) { return 666; }

            for (i = 0; i < state->mesh->group[state->num_groups]->num_tris; i++)
            {

              state->mesh->group[state->num_groups]->tri[i]->v_id[2] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+0)]);
              state->mesh->group[state->num_groups]->tri[i]->v_id[1] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+2)]);
              state->mesh->group[state->num_groups]->tri[i]->v_id[0] = state->offs_vertex + binGetUI16_LE(&data[offset+8+((i*8)+4)]);

              state->mesh->group[state->num_groups]->tri[i]->t_id[0] = state->mesh->group[state->num_groups]->tri[i]->v_id[0];
              state->mesh->group[state->num_groups]->tri[i]->t_id[1] = state->mesh->group[state->num_groups]->tri[i]->v_id[1];
              state->mesh->group[state->num_groups]->tri[i]->t_id[2] = state->mesh->group[state->num_groups]->tri[i]->v_id[2];

            }

          }

          state->num_groups++;

        break;

        /* Any other chunk types are ignored. */

        default:
          /* if (state->mesh) { fprintf(stderr, "[3DS] Ignoring chunk: %04x\n", binGetUI16_LE(&data[offset])); } */
        break;

      }

    }

    /* If necessary, allocate memory at the end of the first pass. */

    if (state->flags == ID_3DS_EDIT3DS && pass == 0)
    {
      state->mesh = morphCreate(1, state->num_verts, 0, state->num_coords, state->num_groups);
      if (!state->mesh) { return 666; }
      state->mesh->mtrl = mtrlCreateList(state->num_mtrls);
      if (!state->mesh->mtrl && state->num_mtrls > 0) { return 666; }
    }

  }

  /* No problems found; report success. */

  return 0;

}

/**
@brief [INTERNAL] Load a 3DS-format mesh.

REFERENCE:

http://paulbourke.net/dataformats/3ds/

http://www.jrepp.com/docs/3ds-0.1.htm

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoad3DS(long length, const unsigned char *data)
{

  mesh_parse_s state;
  unsigned long data_size = 0;

  if      (length <= 6 || !data)                    { return NULL; }
  else if (binGetUI16_LE(data) != ID_3DS_MAIN3DS) { return NULL; }

  data_size = binGetUI32_LE(&data[2]);
  if (data_size > (unsigned long)length) { return NULL; }

  memset(&state, 0, sizeof(mesh_parse_s));
  state.flags = ID_3DS_MAIN3DS;

  if (morphLoad3DS_chunk(&state, data_size-6, &data[6]) == 0)
  { morphGenerateNormals(state.mesh); }
  else
  { state.mesh = morphFree(state.mesh); }

  return state.mesh;

}

/**
@brief [INTERNAL] Load an STL-format mesh.

This function does not handle ASCII-format STL files - it only handles binary
meshes.

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadSTL(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  long t, i;

  /* Validate header. */

  if (!data || length < 134) { return NULL; }

  t = binGetUI32_LE (&data[80]);

  if (length != 84 + (t*50)) { return NULL; }

  /* Create a new mesh structure with one frame and one group. */

  mesh = morphCreate (1, t * 3, 0, 0, 1);

  if (!mesh) { return NULL; }

  /* Create surface. */

  mesh->group[0] = morphNewGroup ("STL", -1, t);

  if (!mesh->group[0])
  {

    morphFree (mesh);

    return NULL;

  }

  /* Read vertices/triangles. NOTE: This assumes 32-bit IEEE float values. */

  for (t = 0; t < mesh->group[0]->num_tris; t++)
  {

    for (i = 0; i < 3; i++)
    {

      mesh->group[0]->tri[t]->v_id[2-i] = (t*3)+i;

      memcpy (&mesh->vertex[0][((t*3)+i)*3], &data[96+(t*50)+(i*12)], 12);

    }

  }

  /* Return the mesh. */

  morphGenerateNormals (mesh);

  return mesh;

}

/**
@brief [INTERNAL] Load a Quake 1 MDL-format mesh.

Only Quake 1 format MDLs are supported. Half Life and Serious Sam MDL files are
ignored. Quake 1 Beta models may be supported in the future.

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadMDL(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;  /* The mesh object to be returned. */
  long         *exuv = NULL;  /* Extra texture coord index list. */

  float scale[3];             /* Vertex compression data: Scaling.     */
  float trans[3];             /* Vertex compression data: Translation. */

  long offs_frames = 0;
  long offs_faces  = 0;
  long offs_coords = 0;

  long num_verts     = 0;     /* Number of vertices per frame. */
  long num_faces     = 0;     /* Number of triangles.          */
  long num_ex_coords = 0;     /* Number of EXTRA texcoords.    */

  long skin_width  = 0;       /* Texture width  (pixels). */
  long skin_height = 0;       /* Texture height (pixels). */

  long tmp = 0;

  long i, j, k, a, b;

  /* Validate header. */

  if (!data || length < 84) { return NULL; }

  if (memcmp (data, "IDPO", 4) != 0) { return NULL; }
  if (binGetUI32_LE (&data[4])  != 6) { return NULL; }

  /* Get vertex and triangle counts. */

  num_verts = binGetUI32_LE (&data[60]);
  num_faces = binGetUI32_LE (&data[64]);

  /* Skip past the embedded textures + Work out the texcoord data offset. */

  tmp         = binGetUI32_LE (&data[48]);
  skin_width  = binGetUI32_LE (&data[52]);
  skin_height = binGetUI32_LE (&data[56]);

  offs_coords = 84;

  for (i = 0; i < tmp; i++)
  {

    offs_coords += 4;

    if (binGetUI32_LE (&data[offs_coords-4]))
    {

      /* Animated texture - get frame count. */

      j = binGetUI32_LE (&data[offs_coords]);

      offs_coords += (skin_width * skin_height * j) + (4 * j) + 4;

    } else { offs_coords += skin_width * skin_height; }

  }

  offs_faces  = offs_coords + (12 * num_verts);
  offs_frames = offs_faces  + (16 * num_faces);

  /* Create a lookup table for "dual purpose" texcoords. */

  exuv = (long*)malloc (sizeof(long) * num_verts);

  if (!exuv) { return NULL; }

  /* Determine how many "dual purpose" texcoords exist. */

  num_ex_coords = 0;

  for (i = 0; i < num_verts; i++)
  {

    if (binGetUI32_LE (&data[offs_coords + (i*12)]))
    {

      exuv[i] = num_verts + num_ex_coords;

      num_ex_coords++;

    } else { exuv[i] = i; }

  }

  /* Create the mesh, with no normal data and a single group. */

  mesh = morphCreate (binGetUI32_LE (&data[68]), num_verts, 0, num_verts + num_ex_coords, 1);

  if (!mesh)
  {

    free (exuv);

    return NULL;

  }

  /* Create the group. */

  mesh->group[0] = morphNewGroup ("MDL", -1, num_faces);

  if (!mesh->group[0])
  {

    morphFree (mesh);

    free (exuv);

    return NULL;

  }

  /* Copy texture coordinate data. */

  for (i = 0; i < num_verts; i++)
  {

    a = binGetUI32_LE (&data[offs_coords + (i*12) + 4]);
    b = binGetUI32_LE (&data[offs_coords + (i*12) + 8]);

    mesh->texcoord[(i*2)+0] =        (float)a / (float)skin_width;
    mesh->texcoord[(i*2)+1] = 1.0f - (float)b / (float)skin_height;

    if (binGetUI32_LE (&data[offs_coords + (i*12)]) != 0)
    {

      mesh->texcoord[(exuv[i]*2)+0] = mesh->texcoord[(i*2)+0] + 0.5f;
      mesh->texcoord[(exuv[i]*2)+1] = mesh->texcoord[(i*2)+1];

    }

  }

  /* Read triangle data. */

  for (i = 0; i < num_faces; i++)
  {

    /* Get vertex index. */

    for (k = 0; k < 3; k++)
    {

      mesh->group[0]->tri[i]->v_id[k] =
        binGetUI32_LE (&data[offs_faces + (i*16) + (k*4) + 4]);

    }

    for (k = 0; k < 3; k++)
    {

      /* Determine whether this texture coordinate index needs to be changed. */

      if (binGetUI32_LE (&data[offs_faces + (i*16)]) != 0)
      {

        mesh->group[0]->tri[i]->t_id[k] =
          mesh->group[0]->tri[i]->v_id[k];

      }
      else
      {

        mesh->group[0]->tri[i]->t_id[k] =
          exuv[mesh->group[0]->tri[i]->v_id[k]];

      }

    }

  }

  /* Free the alternative texture coordinate index table. */

  if (exuv) { free (exuv); }

  /* Get vertex scale/transform data. */

  memcpy (&scale, &data[ 8], sizeof(float) * 3);
  memcpy (&trans, &data[20], sizeof(float) * 3);

  /* Read frame data. */

  k = offs_frames;
  j = 0;

  for (i = 0; i < mesh->num_frames; i++)
  {

    if (j == 0)
    {

      /* Check frame flags. */

      j =  binGetUI32_LE (&data[k]);

      k += 4;

      /* Skip some data if this is a "group" frame. */

      if (j != 0) { k += 8 + (mesh->num_frames * 4); }

    }

    /* Skip some unnecessary data. */

    k += 8;

    /* Get frame name. */

    mesh->frame[i]->name = (char*)malloc(17);

    if (mesh->frame[i]->name)
    {
      memset(mesh->frame[i]->name, 0,        17);
      memcpy(mesh->frame[i]->name, &data[k], 16);
    }

    k += 16;

    /* Copy vertex data. */

    for (a = 0; a < num_verts; a++)
    {

      for (b = 0; b < 3; b++)
      {

        mesh->vertex[i][(a*3) + b] = ((float)data[k+b] * scale[b]) + trans[b];

      }

      k += 4;

    }

  }

  /* Return the mesh. */

  morphGenerateNormals (mesh);

  return mesh;

}

/**
@brief [INTERNAL] Load a Quake 2 MD2-format mesh.

Does what you'd expect.

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadMD2(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;  /* The mesh object to be returned. */

  float scale[3];             /* Vertex compression data: Scaling.     */
  float trans[3];             /* Vertex compression data: Translation. */

  long offs, i, j, k;

  /* Validate header. */

  if (!data || length < 68) { return NULL; }

  if (memcmp (data, "IDP2", 4) != 0) { return NULL; }
  if (binGetUI32_LE (&data[4])  != 8) { return NULL; }

  /* Create mesh and triangle group. */

  mesh = morphCreate (binGetUI32_LE (&data[40]),      /* Anim. Frames.  */
                      binGetUI32_LE (&data[24]), 0,   /* Verts/Norms.   */
                      binGetUI32_LE (&data[28]), 1);  /* Coords/Groups. */

  if (!mesh) { return NULL; }

  mesh->group[0] = morphNewGroup ("MD2", 0, binGetUI32_LE (&data[32]));

  if (!mesh->group[0])
  {

    morphFree (mesh);

    return NULL;

  }

  /* Get texture. MD2 files may contain multiple skins - just use the first. */

  if (binGetUI32_LE (&data[20]) > 0)
  {

    mesh->mtrl = mtrlCreateList (1);

    if (mesh->mtrl)
    {

      memcpy (mesh->mtrl->mtrl[0]->diffusemap,
              &data[binGetUI32_LE (&data[44])], 64);

    }

  }

  /* Get animation data. */

  offs = binGetUI32_LE (&data[56]);

  for (i = 0; i < mesh->num_frames; i++)
  {

    /* Get vertex decompression information. */

    memcpy (scale, &data[offs], 4*3); offs += 4*3;
    memcpy (trans, &data[offs], 4*3); offs += 4*3;

    /* Get frame name. */

    mesh->frame[i]->name = (char*)malloc(17);

    if (mesh->frame[i]->name)
    {
      memset(mesh->frame[i]->name, 0,           17);
      memcpy(mesh->frame[i]->name, &data[offs], 16);
    }

    offs += 16;

    /* Get vertices. Skip the unused Quake 2 normal index. */

    for (j = 0; j < mesh->num_verts; j++)
    {

      for (k = 0; k < 3; k++)
      {

        mesh->vertex[i][(j*3)+k] = ((float)data[offs] * scale[k]) + trans[k];

        offs++;

      }

      offs++;

    }

  }

  /* Get texcoord data. */

  offs = binGetUI32_LE (&data[48]);
  i    = binGetUI32_LE (&data[ 8]);
  j    = binGetUI32_LE (&data[12]);

  for (k = 0; k < mesh->num_coords; k++)
  {

    mesh->texcoord[(k*2)+0] = (0 + data[offs+0]+(data[offs+1]*256)) / (float)i;
    mesh->texcoord[(k*2)+1] = (j - data[offs+2]+(data[offs+3]*256)) / (float)j;

    offs += 4;

  }

  /* Load triangle data. */

  offs = binGetUI32_LE (&data[52]);

  for (i = 0; i < mesh->group[0]->num_tris; i++)
  {

    /* Get vertex indices. */

    for (j = 0; j < 3; j++)
    {

      mesh->group[0]->tri[i]->v_id[j] = data[offs]+(data[offs+1]*256);

      offs += 2;

    }

    /* Get texcoord indices. */

    for (j = 0; j < 3; j++)
    {

      mesh->group[0]->tri[i]->t_id[j] = data[offs]+(data[offs+1]*256);

      offs += 2;

    }

  }

  /* Return the mesh. */

  morphGenerateNormals (mesh);

  return mesh;

}

/**
@brief [INTERNAL] Load a Quake 3 MD3-format mesh.

@todo Could use some tidying up.

@param length The total size of the data[] array.
@param data An array of raw byte values.
@return A new mesh object on success or NULL on failure.
*/

static mesh_s *morphLoadMD3(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  long num_groups, num_frames, num_verts, num_faces;
  long total_verts, offs, sub_offs, i, j, k, frame;
  float lat, lng;

  if (!data) { return NULL; }

  /* Validate header. */

  if (length < MD3_SIZE_HEAD) { return NULL; }

  if      (memcmp(data, "IDP3", 4)                  != 0 ) { return NULL; }
  else if (binGetUI32_LE(&data[MD3_HEAD_VERSION]) != 15) { return NULL; }

  num_frames = binGetUI32_LE(&data[MD3_HEAD_NUM_FRAMES]);
  num_groups = binGetUI32_LE(&data[MD3_HEAD_NUM_SURFACES]);

  /* Count the number of vertices required by this mesh. */

  num_verts = 0;
  offs      = binGetUI32_LE(&data[MD3_HEAD_POS_SURFACES]);

  for (i = 0; i < num_groups; i++)
  {
    num_verts += binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
    offs      += binGetUI32_LE(&data[offs + MD3_SURF_POS_END  ]);
  }

  /* Attempt to allocate memory for the basic mesh structure. */

  mesh = morphCreate(num_frames, num_verts, num_verts, num_verts, num_groups);
  if (!mesh) { return NULL; }

  /* Load the name of each animation frame. */

  offs = binGetUI32_LE(&data[MD3_HEAD_POS_FRAMES]);

  for (i = 0; i < num_frames; i++)
  {

    mesh->frame[i]->name = (char*)malloc(17);

    if (mesh->frame[i]->name)
    {
      memset(mesh->frame[i]->name, 0,                            17);
      memcpy(mesh->frame[i]->name, &data[offs + MD3_FRAME_NAME], 16);
    }

    offs += MD3_SIZE_FRAME;

  }

  /* Load MD3 tags. */

  mesh->tag_list = meshTagSetCreate(binGetUI32_LE(&data[MD3_HEAD_NUM_TAGS]));

  if (mesh->tag_list)
  {

    offs = binGetUI32_LE(&data[MD3_HEAD_POS_TAGS]);

    for (i = 0; i < mesh->tag_list->num_tags; i++)
    {

      /* FIXME: This assumes the name is >= 64 bytes long - It MAY be less! */

      memcpy(mesh->tag_list->tag[i]->name,   &data[offs + MD3_TAG_NAME  ], 64   );
      memcpy(mesh->tag_list->tag[i]->origin, &data[offs + MD3_TAG_ORIGIN], 4 * 3);

      /* TODO: Load the 3x3 matrix data here. */

      offs += MD3_SIZE_TAG;

    }

  } else { /* TODO: Flag this as an error. */ }

  /* Initialise materials list. */

  mesh->mtrl = mtrlCreateList(num_groups);

  if (!mesh->mtrl) { /* TODO: Flag this as an error. */ }

  /* Load each group in turn. */

  total_verts = 0;

  offs = binGetUI32_LE(&data[MD3_HEAD_POS_SURFACES]);

  for (i = 0; i < num_groups; i++)
  {

    if (mesh->mtrl && binGetUI32_LE(&data[offs + MD3_SURF_NUM_SKINS]) > 0)
    {

      /* FIXME: This assumes diffusemap is >= 64 bytes long - It MAY be less! */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_SKINS]);

      memcpy(mesh->mtrl->mtrl[i]->diffusemap, &data[offs + sub_offs], 64);

    }

    /* FIXME: This assumes that name[] is >= 64 bytes long - It MAY be less! */

    num_faces = binGetUI32_LE(&data[offs + MD3_SURF_NUM_FACES]);

    mesh->group[i] = morphNewGroup((const char*)&data[offs + MD3_SURF_NAME], i, num_faces);

    if (mesh->group[i])
    {

      /* Load triangles. */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_FACES]);

      for (j = 0; j < num_faces; j++)
      {

        for (k = 0; k < 3; k++)
        {

          mesh->group[i]->tri[j]->v_id[k] = binGetUI32_LE(&data[offs + sub_offs]);
          mesh->group[i]->tri[j]->v_id[k] += total_verts;

          mesh->group[i]->tri[j]->n_id[k] = mesh->group[i]->tri[j]->v_id[k];
          mesh->group[i]->tri[j]->t_id[k] = mesh->group[i]->tri[j]->v_id[k];

          sub_offs += 4;

        }

      }

      /* Load vertices and normals. */

      num_verts = binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
      sub_offs  = binGetUI32_LE(&data[offs + MD3_SURF_POS_VERTS]);

      for (frame = 0; frame < num_frames; frame++)
      {

        for (j = 0; j < num_verts; j++)
        {

          /* Get the XYZ vertex position. */

          for (k = 0; k < 3; k++)
          {

            mesh->vertex[frame][(total_verts * 3) + (j * 3) + k] =
                MD3_XYZ_SCALE * (float)binGetSI16_LE(&data[offs + sub_offs]);

            sub_offs += 2;

          }

          /* Decode the normal (convert "lat"/"lng" from 0-255 into radians). */

          lng = (double)data[offs + sub_offs + 0] / 255.0 * (2.0 * 3.141593);
          lat = (double)data[offs + sub_offs + 1] / 255.0 * (2.0 * 3.141593);

          mesh->normal[frame][(total_verts * 3) + (j * 3) + 0] = cos(lat) * sin(lng);
          mesh->normal[frame][(total_verts * 3) + (j * 3) + 1] = sin(lat) * sin(lng);
          mesh->normal[frame][(total_verts * 3) + (j * 3) + 2] = cos(lng);

          sub_offs += 2;

        }

      }

      /* Load texcoords. */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_COORDS]);

      for (j = 0; j < num_verts; j++)
      {

        memcpy(&mesh->texcoord[(total_verts * 2) + (j * 2) + 0], &data[offs + sub_offs + 0], 4);
        memcpy(&mesh->texcoord[(total_verts * 2) + (j * 2) + 1], &data[offs + sub_offs + 4], 4);

        mesh->texcoord[(total_verts * 2) + (j*2) + 1] =
            1.0f - mesh->texcoord[(total_verts * 2) + (j * 2) + 1];

        sub_offs += 8;

      }

    } else { /* TODO: Flag this as an error. */ }

    total_verts += binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
    offs        += binGetUI32_LE(&data[offs + MD3_SURF_POS_END  ]);

  }

  /* Return result. */

  return mesh;

}

/**
@brief [INTERNAL] Per-line callback function for parsing Doom 3 .md5mesh files.

This function is called before a mesh has been allocated, in order to determine
how much memory will be required. It also builds a list of weight information.

@param line The current line of text.
@param data Points to the current parser state object.
@return Zero on success, or non-zero on error.
*/

static int morphLoadMD5_pass1(const char *line, void *data)
{

  mesh_parse_s *state = (mesh_parse_s*)data;

  if      (!state     ) { return -1; }
  else if (state->mesh) { return -2; }

  if (strncmp(line, "numweights ", 11) == 0)
  {

    const long old_count = state->num_weights;
    void *tmp = NULL;

    state->num_weights += atoi(&line[11]);

    tmp = realloc(state->weight, sizeof(mesh_weight_s**) * state->num_weights);
    if (!tmp) { return 666; }
    state->weight = (mesh_weight_s**)tmp;
    memset(&state->weight[old_count], 0, sizeof(mesh_weight_s**) * (state->num_weights - old_count));

  }
  else if (strncmp(line, "weight ", 7) == 0)
  {

    int local_weight_number = 0;

    if (state->next_weight >= state->num_weights) { return -3; }

    state->weight[state->next_weight] = (mesh_weight_s*)malloc(sizeof(mesh_weight_s));
    if (!state->weight[state->next_weight]) { return 666; }
    memset(state->weight[state->next_weight], 0, sizeof(mesh_weight_s));

    sscanf(&line[7], "%d %ld %f (%f %f %f)", &local_weight_number,
        &state->weight[state->next_weight]->joint_id,
        &state->weight[state->next_weight]->bias,
        &state->weight[state->next_weight]->xyz[0],
        &state->weight[state->next_weight]->xyz[1],
        &state->weight[state->next_weight]->xyz[2]);

    state->next_weight++;

  }
  else if (strncmp(line, "numJoints ", 10) == 0) { state->num_joints += atoi(&line[10]); }
  else if (strncmp(line, "numMeshes ", 10) == 0) { state->num_groups += atoi(&line[10]); }
  else if (strncmp(line, "numverts ",   9) == 0) { state->num_verts  += atoi(&line[ 9]); }

  return 0;

}

/**
@brief [INTERNAL] Per-line callback function for parsing Doom 3 .md5mesh files.

This function is called once a basic mesh structure has been allocated, and is
used to fill in the (initially blank) structure with the mesh information.

@param line The current line of text.
@param data Points to the current parser state object.
@return Zero on success, or non-zero on error.
*/

static int morphLoadMD5_pass2(const char *line, void *data)
{

  mesh_parse_s *state = (mesh_parse_s*)data;
  int temp = 0;

  if      (!state      ) { return -1; }
  else if (!state->mesh) { return -2; }

  if      (strncmp(line, "joints", 6) == 0) { state->flags |= MESH_PARSE_FLAG_MD5JOINTS; }
  else if (line[0] == '}'                 ) { state->flags = 0;                           }
  else if (state->flags & MESH_PARSE_FLAG_MD5JOINTS)
  {

    /* If there are more joints than originally specified, it's an error. */
    if (state->num_joints >= state->mesh->tag_list->num_tags) { return 1; }

    /* Copy the joint data into memory. */
    sscanf(line, "\"%[^\"]\" %ld ( %f %f %f ) ( %f %f %f )",
        state->mesh->tag_list->tag[state->num_joints]->name,
        &state->mesh->tag_list->tag[state->num_joints]->parent,
        &state->mesh->tag_list->tag[state->num_joints]->origin[0],
        &state->mesh->tag_list->tag[state->num_joints]->origin[1],
        &state->mesh->tag_list->tag[state->num_joints]->origin[2],
        &state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_X],
        &state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_Y],
        &state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_Z]);

    /* Calculate the missing W-component of the quaternion. */
    state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_W] = quatCalculateW(
        state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_X],
        state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_Y],
        state->mesh->tag_list->tag[state->num_joints]->quaternion[QUAT_Z]);

    /* Increment the joint counter. */
    state->num_joints++;

  }
  else if (strncmp(line, "numtris ", 8) == 0)
  {

    /* If no group is declared (or the group already exists), it's an error. */
    if      (state->num_groups <= 0                 ) { return 1; }
    else if (state->mesh->group[state->num_groups-1]) { return 2; }

    /* Otherwise, create a new (empty) triangle group. */
    state->mesh->group[state->num_groups-1] = morphNewGroup(NULL, state->num_groups-1, atof(&line[8]));
    if (!state->mesh->group[state->num_groups-1]) { return 666; }

    /* Reset the (per-group) triangle counter. */
    state->num_tris = 0;

  }
  else if (strncmp(line, "tri ", 4) == 0)
  {

    mesh_group_s *group = NULL;

    /* Attempt to get the current triangle group. */
    if (state->num_groups <= 0) { return 1; }
    group = state->mesh->group[state->num_groups-1];

    /* If the group doesn't exist or is already at max capacity, report it. */
    if      (!group                            ) { return 2; }
    else if (state->num_tris >= group->num_tris) { return 3; }

    /* Get the vertex indices for this triangle. */
    sscanf(&line[4], "%d %ld %ld %ld", &temp,
        &group->tri[state->num_tris]->v_id[0],
        &group->tri[state->num_tris]->v_id[1],
        &group->tri[state->num_tris]->v_id[2]);

    /* The vertex indices are local to this mesh, so correct them. */
    group->tri[state->num_tris]->v_id[0] += state->offs_vertex;
    group->tri[state->num_tris]->v_id[1] += state->offs_vertex;
    group->tri[state->num_tris]->v_id[2] += state->offs_vertex;

    /* The texcoord IDs are the same as the vertex IDs, so just copy them. */
    group->tri[state->num_tris]->t_id[0] = group->tri[state->num_tris]->v_id[0];
    group->tri[state->num_tris]->t_id[1] = group->tri[state->num_tris]->v_id[1];
    group->tri[state->num_tris]->t_id[2] = group->tri[state->num_tris]->v_id[2];

    /* Increment the triangle counter. */
    state->num_tris++;

  }
  else if (strncmp(line, "vert ", 5) == 0)
  {

    long weight_start, weight_count, i;

    if (state->num_verts >= state->mesh->num_verts) { return 1; }

    /* Copy the texture coordinate and vertex weight index data. */
    sscanf(&line[5], "%d ( %f %f ) %ld %ld", &temp,
        &state->mesh->texcoord[(state->num_verts*2)+0],
        &state->mesh->texcoord[(state->num_verts*2)+1], &weight_start, &weight_count);

    /* TODO: Check that weight_start and weight_count are within range... */
    for (i = 0; i < weight_count; i++)
    {

      const mesh_weight_s *weight = state->weight[state->next_weight + weight_start + i];
      mesh_tag_s *joint = state->mesh->tag_list->tag[weight->joint_id];

      quaternion_t original_position, final_position;

      original_position[QUAT_W] = 0;
      original_position[QUAT_X] = weight->xyz[0];
      original_position[QUAT_Y] = weight->xyz[1];
      original_position[QUAT_Z] = weight->xyz[2];

      quatRotate(final_position, original_position, joint->quaternion);

      state->mesh->vertex[0][(state->num_verts*3)+0] += (final_position[QUAT_X] + joint->origin[0]) * weight->bias;
      state->mesh->vertex[0][(state->num_verts*3)+1] += (final_position[QUAT_Y] + joint->origin[1]) * weight->bias;
      state->mesh->vertex[0][(state->num_verts*3)+2] += (final_position[QUAT_Z] + joint->origin[2]) * weight->bias;

    }

    /* Increment the vertex/texture coordinate counter. */
    state->num_verts++;

  }
  else if (strncmp(line, "mesh", 4) == 0)
  {
    if (state->num_groups >= state->mesh->num_groups) { return 1; }
    state->num_groups++;  /* <-- Note that this stores the NEXT group index! */
  }
  else if (strncmp(line, "numverts ",    9) == 0) { state->offs_vertex = state->num_verts; }
  else if (strncmp(line, "numweights ", 11) == 0) { state->next_weight += atoi(&line[11]); }

  /* Report success - no problems found. */
  return 0;

}

/**
@brief [INTERNAL] Load a Doom 3 .md5mesh file.

Note that this does not handle .md5anim or .md5camera files.

@todo Binary MD5 files (as seen in Quake Wars: Enemy Territory).

REFERENCE:

http://tfc.duke.free.fr/coding/md5-specs-en.html

@param text A NULL-terminated C string describing an MD5 mesh.
@return A valid morph mesh structure on success, or NULL on failure.
*/

static mesh_s *morphLoadMD5(const char *text)
{

  mesh_parse_s state;
  int error = 0;
  long i = 0;

  if (!text) { return NULL; }
  if (strncmp(text, MD5_VERSION, strlen(MD5_VERSION)) != 0) { return NULL; }

  /* FIRST PASS: Determine how much memory will be required and allocate it. */

  memset(&state, 0, sizeof(mesh_parse_s));
  error = txtEachLine(text, "//", morphLoadMD5_pass1, &state);

  if (error == 0)
  {

    state.mesh = morphCreate(1, state.num_verts, 0, state.num_verts, state.num_groups);

    if (state.mesh)
    {

      state.mesh->tag_list = meshTagSetCreate(state.num_joints);

      if (state.mesh->tag_list)
      {

        /* SECOND PASS: Load the data into the allocated mesh structure. */

        state.num_joints = 0;
        state.num_groups = 0;
        state.num_verts = 0;
        state.next_weight = 0;

        error = txtEachLine(text, "//", morphLoadMD5_pass2, &state);

      } else { error = 66; }

    } else { error = 99; }

  }

  /* FINALLY: Free the weights array, as it's no longer needed. */

  if (state.weight)
  {

    for (i = 0; i < state.num_weights; i++)
    {
      if (state.weight[i]) { free(state.weight[i]); }
    }

    free(state.weight);

  }

  /* If any errors were encountered, free the partially-loaded mesh data. */

  if (error != 0) { return morphFree(state.mesh); }

  /* Generate normals and return the result. */

  morphGenerateNormals(state.mesh);
  return state.mesh;

}

/**
@brief [INTERNAL] Load a Princeton OFF (Object File Format) mesh from text.

Sample OFF files can be downloaded from the Princeton Shape Benchmark site:

http://shape.cs.princeton.edu/benchmark/

Note that those OFF files "can only be used for academic purposes and cannot be
used for commercial products without permission."

REFERENCE:

http://shape.cs.princeton.edu/benchmark/documentation/off_format.html

@param text A NULL-terminated C string describing an OFF mesh.
@return A valid morph mesh structure on success, or NULL on failure.
*/

static mesh_s *morphLoadOFF(const char *text)
{

  mesh_s *mesh = NULL;
  char *text_end = NULL;
  const char *face_start = NULL;
  long num_verts, num_faces, num_edges, num_tris, i, pass;
  int error = 0;

  if (!text) { return NULL; }

  /* The file needs to start with "OFF" in order to be an OFF file. */

  if (strncmp(text, "OFF", 3) != 0) { return NULL; }
  if (!isspace(text[3])) { return NULL; }
  text += 4;

  /* The first 3 integers represent numVertices, numFaces, and numEdges. */

  num_verts = strtol(text, &text_end, 0);
  if (!text_end) { return NULL; } else { text = text_end; }

  num_faces = strtol(text, &text_end, 0);
  if (!text_end) { return NULL; } else { text = text_end; }

  num_edges = strtol(text, &text_end, 0);
  if (!text_end || num_edges != 0) { return NULL; } else { text = text_end; }

  /* Allocate memory for the main mesh structure. */

  mesh = morphCreate(1, num_verts, 0, 0, 1);
  if (!mesh) { return NULL; }

  /* Load vertices. */

  for (i = 0; i < num_verts * 3; i++)
  {
    mesh->vertex[0][i] = strtod(text, &text_end);
    if (!text_end) { error = 1; } else { text = text_end; }
  }

  /* Load faces. These might not be triangles, so conversions are necessary. */

  for (face_start = text, pass = 0; pass < 2; pass++, text = face_start)
  {

    for (num_tris = 0, i = 0; i < num_faces; i++)
    {

      long points, j;

      /* Get the number of points in the face. */

      points = strtol(text, &text_end, 0);
      if (!text_end || points < 3) { error = 1; } else { text = text_end; }

      for (j = 0; j < points; j++)
      {

        /* Get the vertex index of this point. */

        long v_id = strtol(text, &text_end, 0);
        if (!text_end) { error = 1; } else { text = text_end; }

        /* Determine if this is a basic triangle, or a quad/etc. */

        if (j < 3)
        {
          if (mesh->group[0]) { mesh->group[0]->tri[num_tris]->v_id[j] = v_id; }
        }
        else
        {

          /* Every extra vertex (after the third) requires a new triangle. */

          num_tris++;

          if (mesh->group[0])
          {
            mesh->group[0]->tri[num_tris]->v_id[0] = mesh->group[0]->tri[num_tris-1]->v_id[0];
            mesh->group[0]->tri[num_tris]->v_id[1] = mesh->group[0]->tri[num_tris-1]->v_id[2];
            mesh->group[0]->tri[num_tris]->v_id[2] = v_id;
          }

        }

      }

      /* Move on to the next triangle. */

      num_tris++;

    }

    /* At the end of the first pass, allocate memory to store the triangles. */

    if (pass == 0)
    {
      mesh->group[0] = morphNewGroup(NULL, -1, num_tris);
      if (!mesh->group) { error = 1; }
    }

  }

  /* If any errors were encountered, free the partially-loaded mesh data. */

  if (error != 0) { return morphFree(mesh); }

  /* Reverse the face-winding order, generate normals and return the result. */

  morphReverseFaces(mesh);
  morphGenerateNormals(mesh);
  return mesh;

}

/**
@brief [INTERNAL] Per-line callback used when parsing OBJ files.

State is stored in a structure that is passed through to each call as userdata.

@param line Single line of pre-processed text (extra spaces/tabs removed, etc).
@param data Userdata: Used to pass through the "mesh_parse_s" state object.
@return Zero on success, or non-zero on error.
*/

static int morphReadLineOBJ(const char *line, void *data)
{

  mesh_parse_s *pso = NULL;
  mesh_group_s *grp = NULL;
  char          *str = NULL;
  char          *tmp = NULL;

  long v_i, t_i, n_i, i;

  /* Get the parse state object. */

  pso = (mesh_parse_s*)data;

  if (!pso) { return -1; }

  /* Count things. Faces BEFORE the first group are in the "default" group. */

  if (!strncmp (line, "g ", 2) || (!strncmp (line, "f ", 2) && pso->num_groups < 1))
  {

    if (pso->mesh)
    {

      /* Get the name of the NEXT group. */

      grp = pso->mesh->group[pso->num_groups];

      if (grp && line[0] == 'g')
      {
        if (grp->name) { free(grp->name); }
        grp->name = (char*)malloc(strlen(&line[2]) + 1);
        if (grp->name) { strcpy(grp->name, &line[2]); }
      }

      /* Create the PREVIOUS group. */

      if (pso->num_groups > 0)
      {

        if (!pso->mesh->group[pso->num_groups-1])
        {

          pso->mesh->group[pso->num_groups-1] = morphNewGroup (NULL, -1, pso->num_tris);

        }

      }

    }

    /* Increment the group counter and reset the per-group triangle counter. */

    pso->num_tris = 0;
    pso->num_groups++;

  }
  else if (!strncmp (line, "v ",  2))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[2], "%f %f %f", &pso->mesh->vertex[0][(pso->num_verts*3)+0],
                                    &pso->mesh->vertex[0][(pso->num_verts*3)+1],
                                    &pso->mesh->vertex[0][(pso->num_verts*3)+2]);

    }

    /* Increment the vertex counter. */

    pso->num_verts++;

  }
  else if (!strncmp (line, "vt ", 3))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[3], "%f %f", &pso->mesh->texcoord[(pso->num_coords*2)+0],
                                 &pso->mesh->texcoord[(pso->num_coords*2)+1]);

    }

    /* Increment the texcoord counter. */

    pso->num_coords++;

  }
  else if (!strncmp (line, "vn ", 3))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[3], "%f %f %f", &pso->mesh->normal[0][(pso->num_norms*3)+0],
                                    &pso->mesh->normal[0][(pso->num_norms*3)+1],
                                    &pso->mesh->normal[0][(pso->num_norms*3)+2]);

      /* Invert the normals, as we will reverse all faces later on. */

      for (i = 0; i < 3; i++)
      {

        pso->mesh->normal[0][(pso->num_norms*3)+i] =
        -pso->mesh->normal[0][(pso->num_norms*3)+i];

      }

    }

    /* Increment the normal counter. */

    pso->num_norms++;

  }

  /* The next part of the code relies on the mesh already being allocated. */

  if (!pso->mesh) { return 0; }

  /* Check for material data. */

  if (pso->mesh->mtrl)
  {

    if (!strncmp (line, "usemtl ", 7))
    {

      pso->mat_id = mtrlFind (pso->mesh->mtrl, &line[7]);

    }

  }
  else if (!strncmp (line, "mtllib ", 7))
  {

    pso->mesh->mtrl = mtrlLoadFile (&line[7]);

  }

  /* Check for face data. */

  if (!strncmp (line, "f ", 2))
  {

    /* Count the number of triangles in the current face. */

    str = (char*)line;
    i   = 0;

    while (str)
    {

      str = strstr (&str[1], " ");

      if (str)
      {

        /* Initialise the vertex, texture, and normal indices. */

        v_i = 0;
        t_i = 0;
        n_i = 0;

        /* Get the vertex index. */

        if (sscanf (&str[1], "%ld", &v_i) == 1)
        {

          /* Check for other indices. */

          tmp = strstr (&str[1], "/");

          if (tmp)
          {

            /* Get the texture coordinate index. */

            if (tmp[1] != '/')
            {

              if (sscanf (&tmp[1], "%ld", &t_i) != 1) { t_i = 0; }

            }

            /* Get the normal index. */

            tmp = strstr (&tmp[1], "/");

            if (tmp)
            {

              if (sscanf (&tmp[1], "%ld", &n_i) != 1) { n_i = 0; }

            }

          }

          /* OBJ files start from index one, not index zero. */

          v_i--;
          t_i--;
          n_i--;

          /* Copy the face indices, if the group exists. */

          grp = pso->mesh->group[pso->num_groups-1];

          if (grp)
          {

            /* Some tools put "usemtl" before "g abc" and some put it after. */

            grp->mat_id = pso->mat_id;

            /* Set triangle indices. */

            if (i < 3)
            {

              grp->tri[pso->num_tris]->v_id[i] = v_i;
              grp->tri[pso->num_tris]->n_id[i] = n_i;
              grp->tri[pso->num_tris]->t_id[i] = t_i;

            }
            else
            {

              grp->tri[pso->num_tris+i-2]->v_id[0] =
              grp->tri[pso->num_tris+i-3]->v_id[0];
              grp->tri[pso->num_tris+i-2]->v_id[1] =
              grp->tri[pso->num_tris+i-3]->v_id[2];
              grp->tri[pso->num_tris+i-2]->v_id[2] = v_i;

              grp->tri[pso->num_tris+i-2]->n_id[0] =
              grp->tri[pso->num_tris+i-3]->n_id[0];
              grp->tri[pso->num_tris+i-2]->n_id[1] =
              grp->tri[pso->num_tris+i-3]->n_id[2];
              grp->tri[pso->num_tris+i-2]->n_id[2] = n_i;

              grp->tri[pso->num_tris+i-2]->t_id[0] =
              grp->tri[pso->num_tris+i-3]->t_id[0];
              grp->tri[pso->num_tris+i-2]->t_id[1] =
              grp->tri[pso->num_tris+i-3]->t_id[2];
              grp->tri[pso->num_tris+i-2]->t_id[2] = t_i;

            }

          }

          /* Increment vertex counter */

          i++;

        }

      }

    }

    /* Increment the face counter. */

    if (i >= 3) { pso->num_tris += i - 2; }

  }

  return 0;

}

/**
@brief [INTERNAL] Load an OBJ file from string of text.

The data is read in multiple passes:
<ul>
<li>Count Groups, Verts, Normals, Texcoords. Allocate the mesh once done.</li>
<li>Look for MTLLIB, Load Verts/Norms/Coords, Count Faces, Create Groups.</li>
<li>Look for USEMTL, Load Triangles and Group Names.</li>
</ul>
The actual reading is handled by a calback function: morphReadLineOBJ()

@param text The text buffer to parse.
@return A valid mesh on success or NULL on failure.
*/

static mesh_s *morphLoadOBJ(const char *text)
{

  mesh_parse_s state;
  int i = 0;

  if (!text) { return NULL; }

  /* Loop through the text 3 times, loading different data on each pass. */

  state.mesh = NULL;

  for (i = 0; i < 3; i++)
  {

    mesh_s *temp = state.mesh;
    memset(&state, 0, sizeof(mesh_parse_s));
    state.mesh = temp;

    state.mat_id = -1;

    txtEachLine(text, "#", morphReadLineOBJ, &state);

    if (!state.mesh)
    {

      /* Create the main mesh structure at the end of the first pass. */

      state.mesh = morphCreate (1, state.num_verts,  state.num_norms,
                                   state.num_coords, state.num_groups);

      if (!state.mesh) { return NULL; }

    }
    else if (!state.mesh->group[state.num_groups-1])
    {

      /* Create the FINAL group at the end of the second pass. */

      state.mesh->group[state.num_groups-1] = morphNewGroup (NULL, -1, state.num_tris);

    }

  }

  /* Reverse the faces BEFORE generating normals! */

  morphReverseFaces    (state.mesh);
  morphGenerateNormals (state.mesh);

  /* Return the loaded mesh. */

  return state.mesh;

}

/*
[PUBLIC] Load a mesh from string.
*/

mesh_s *morphLoadText(const char *text)
{

  mesh_s *mesh = NULL;

  if (!text) { return NULL; }

  mesh = morphLoadMD5(text);

  if (!mesh) { mesh = morphLoadOFF(text); }
  if (!mesh) { mesh = morphLoadOBJ(text); }

  morphCalculateAABB(mesh);
  morphCalculateTagAABB(mesh, 0);
  if (mesh) { mtrlUnixPaths(mesh->mtrl); }

  return mesh;

}

/*
[PUBLIC] Load a morph mesh from a data buffer.
*/

mesh_s *morphLoadData(long length, const unsigned char *data)
{

  mesh_s *mesh = NULL;

  if (length < 1 || !data) { return NULL; }

  /* Attempt to load a mesh from binary format data. */

  mesh = morphLoadMS3D(length, data);

  if (!mesh) { mesh = morphLoadLWO(length, data); }
  if (!mesh) { mesh = morphLoad3DS(length, data); }
  if (!mesh) { mesh = morphLoadMD3(length, data); }
  if (!mesh) { mesh = morphLoadMD2(length, data); }
  if (!mesh) { mesh = morphLoadMDL(length, data); }
  if (!mesh) { mesh = morphLoadSTL(length, data); }

  /* Return result. */

  morphCalculateAABB(mesh);
  morphCalculateTagAABB(mesh, 0);
  if (mesh) { mtrlUnixPaths(mesh->mtrl); }

  return mesh;

}

/*
[PUBLIC] Load a morph mesh from a file.
*/

mesh_s *morphLoadFile(const char *file_name)
{

  mesh_s  *mesh = NULL;
  unsigned char *data = NULL;
  long length = 0;

  data = txtLoadFile(file_name, &length);

  mesh = morphLoadData(length, data);
  if (!mesh) { mesh = morphLoadText((const char*)data); }

  if (data) { free(data); }

  return mesh;

}
