/*

FILENAME: mesh_util.c
AUTHOR/S: Thomas Dennis
CREATION: Sometime 2008

Copyright (c) 2008-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file mesh_util.c

@brief Provides utilities for loading morph-animated mesh files.

Static formats (such as OBJ or STL) are also supported.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mesh.h"

/*
[PUBLIC] Allocate memory for a single animation frame structure.
*/

mesh_tag_set_s *meshTagSetCreate(long num_tags)
{

  mesh_tag_set_s *frame = NULL;
  long i = 0;

  if (num_tags < 1) { return NULL; }

  frame = (mesh_tag_set_s*)malloc(sizeof(mesh_tag_set_s));
  if (!frame) { return NULL; }
  memset(frame, 0, sizeof(mesh_tag_set_s));

  frame->num_tags = num_tags;

  frame->tag = (mesh_tag_s**)malloc(sizeof(mesh_tag_s*) * num_tags);
  if (!frame->tag) { return meshTagSetDelete(frame); }
  memset(frame->tag, 0, sizeof(mesh_tag_s*) * num_tags);

  for (i = 0; i < num_tags; i++)
  {

    frame->tag[i] = (mesh_tag_s*)malloc(sizeof(mesh_tag_s));
    if (!frame->tag[i]) { return meshTagSetDelete(frame); }
    memset(frame->tag[i], 0, sizeof(mesh_tag_s));

    frame->tag[i]->parent = -1;
    quatIdentity(frame->tag[i]->quaternion);

  }

  return frame;

}

/*
[PUBLIC] Free a previously-created animation frame structure from memory.
*/

mesh_tag_set_s *meshTagSetDelete(mesh_tag_set_s *frame)
{

  long i = 0;

  if (!frame) { return NULL; }

  if (frame->tag)
  {

    for (i = 0; i < frame->num_tags; i++)
    {
      if (frame->tag[i]) { free(frame->tag[i]); }
    }

    free(frame->tag);

  }

  free(frame);
  return NULL;

}

/*
[PUBLIC] Free a previously loaded Morph Mesh.
*/

mesh_s *morphFree(mesh_s *mesh)
{

  long i, j;

  if (!mesh) { return NULL; }

  /* Before doing anything else, free any allocated "working" buffers. */

  meshDeleteBuffers(mesh);

  /* Free groups. */

  if (mesh->group)
  {

    for (i = 0; i < mesh->num_groups; i++)
    {

      if (mesh->group[i])
      {

        if (mesh->group[i]->name) { free(mesh->group[i]->name); }

        /* Free triangles. */

        if (mesh->group[i]->tri)
        {

          for (j = 0; j < mesh->group[i]->num_tris; j++)
          {

            if (mesh->group[i]->tri[j]) { free (mesh->group[i]->tri[j]); }

          }

          free (mesh->group[i]->tri);

        }

        free (mesh->group[i]);

      }

    }

    free (mesh->group);

  }

  /* Free texcoords. */

  if (mesh->texcoord) { free (mesh->texcoord); }

  /* Free normals. */

  if (mesh->normal)
  {

    for (i = 0; i < mesh->num_frames; i++)
    {

      if (mesh->normal[i]) { free (mesh->normal[i]); }

    }

    free (mesh->normal);

  }

  /* Free vertices. */

  if (mesh->vertex)
  {

    for (i = 0; i < mesh->num_frames; i++)
    {

      if (mesh->vertex[i]) { free (mesh->vertex[i]); }

    }

    free (mesh->vertex);

  }

  /* Free frames. */

  if (mesh->frame)
  {

    for (i = 0; i < mesh->num_frames; i++)
    {

      if (mesh->frame[i])
      {

        if (mesh->frame[i]->name) { free(mesh->frame[i]->name); }

        free (mesh->frame[i]);

      }

    }

    free (mesh->frame);

  }

  /* Free tags. */

  meshTagSetDelete(mesh->tag_list);

  if (mesh->vert_tag) { free(mesh->vert_tag); }

  /* Free the main mesh structure. */

  free(mesh);
  return NULL;

}

/*
[PUBLIC] about
*/

int morphCalculateAABB(mesh_s *mesh)
{

  long f, v, i;

  if (!mesh) { return -1; }

  for (f = 0; f < mesh->num_frames; f++)
  {

    for (v = 0; v < mesh->num_verts; v++)
    {

      for (i = 0; i < 3; i++)
      {

        if (v == 0 || mesh->frame[f]->xyzmin[i] > mesh->vertex[f][(v*3)+i])
                    { mesh->frame[f]->xyzmin[i] = mesh->vertex[f][(v*3)+i]; }
        if (v == 0 || mesh->frame[f]->xyzmax[i] < mesh->vertex[f][(v*3)+i])
                    { mesh->frame[f]->xyzmax[i] = mesh->vertex[f][(v*3)+i]; }

      }

    }

  }

  return 0;

}

/*
[PUBLIC] about
*/

int morphCalculateTagAABB(mesh_s *mesh, long frame)
{

  long t, v, axis, got_one;

  /* The mesh must associate vertices with tags in order for this to work... */

  if      (!mesh           || frame < 0                ) { return -1; }
  else if (!mesh->vert_tag || frame >= mesh->num_frames) { return -2; }

  /* Loop through each tag in the mesh. */

  for (t = 0; t < mesh->tag_list->num_tags; t++)
  {

    for (axis = 0; axis < 3; axis++)
    {

      mesh->tag_list->tag[t]->xyzmin[axis] = mesh->tag_list->tag[t]->origin[axis];
      mesh->tag_list->tag[t]->xyzmax[axis] = mesh->tag_list->tag[t]->origin[axis];

      /* Try to obtain the min/max vertex positions associated with the tag. */

      for (got_one = 0, v = 0; v < mesh->num_verts; v++)
      {

        float value = mesh->vertex[frame][(v*3) + axis];

        if (mesh->vert_tag[v] != t) { continue; }

        if (!got_one || value < mesh->tag_list->tag[t]->xyzmin[axis]) { mesh->tag_list->tag[t]->xyzmin[axis] = value; }
        if (!got_one || value > mesh->tag_list->tag[t]->xyzmax[axis]) { mesh->tag_list->tag[t]->xyzmax[axis] = value; }

        got_one = 1;

      }

    }

  }

  /* Report success. */

  return 0;

}

/*
[PUBLIC] about
*/

int morphInvertNormals(mesh_s *mesh)
{

  long i, j, k;

  if      (!mesh        ) { return -1; }
  else if (!mesh->normal) { return 0;  }

  for (i = 0; i < mesh->num_frames; i++)
  {
    for (j = 0; j < mesh->num_norms; j++)
    {
      for (k = 0; k < 3; k++) { mesh->normal[i][(j*3)+k] = -mesh->normal[i][(j*3)+k]; }
    }
  }

  return 0;

}

/*
[PUBLIC] Reverse the face order of a given Morph Mesh.
*/

int morphReverseFaces(mesh_s *mesh)
{

  mesh_group_s *s = NULL;
  long i, j, k;

  if (!mesh) { return -1; }

  /* Loop through all triangles. */

  for (i = 0; i < mesh->num_groups; i++)
  {

    s = mesh->group[i];

    for (j = 0; j < s->num_tris; j++)
    {

      k                  = s->tri[j]->v_id[0];
      s->tri[j]->v_id[0] = s->tri[j]->v_id[2];
      s->tri[j]->v_id[2] = k;

      k                  = s->tri[j]->n_id[0];
      s->tri[j]->n_id[0] = s->tri[j]->n_id[2];
      s->tri[j]->n_id[2] = k;

      k                  = s->tri[j]->t_id[0];
      s->tri[j]->t_id[0] = s->tri[j]->t_id[2];
      s->tri[j]->t_id[2] = k;

    }

  }

  return morphInvertNormals(mesh);

}

/*
[PUBLIC] about
*/

int meshScale(mesh_s *mesh, float x, float y, float z)
{

  long f, i;

  if (!mesh) { return -1; }

  /* Update each animation frame. */

  for (f = 0; f < mesh->num_frames; f++)
  {

    /* Update the AABB values. */

    mesh->frame[f]->xyzmin[0] *= x; mesh->frame[f]->xyzmax[0] *= x;
    mesh->frame[f]->xyzmin[1] *= y; mesh->frame[f]->xyzmax[1] *= y;
    mesh->frame[f]->xyzmin[2] *= z; mesh->frame[f]->xyzmax[2] *= z;

    /* Update the vertex positions. */

    for (i = 0; i < mesh->num_verts; i++)
    {
      mesh->vertex[f][(i*3)+0] *= x;
      mesh->vertex[f][(i*3)+1] *= y;
      mesh->vertex[f][(i*3)+2] *= z;
    }

  }

  /* Update the joint positions. */

  if (mesh->tag_list)
  {

    for (i = 0; i < mesh->tag_list->num_tags; i++)
    {

      mesh->tag_list->tag[i]->origin[0] *= x;
      mesh->tag_list->tag[i]->origin[1] *= y;
      mesh->tag_list->tag[i]->origin[2] *= z;

      mesh->tag_list->tag[i]->xyzmin[0] *= x;
      mesh->tag_list->tag[i]->xyzmin[1] *= y;
      mesh->tag_list->tag[i]->xyzmin[2] *= z;

      mesh->tag_list->tag[i]->xyzmax[0] *= x;
      mesh->tag_list->tag[i]->xyzmax[1] *= y;
      mesh->tag_list->tag[i]->xyzmax[2] *= z;

    }

  }

  return 0;

}

/*
[PUBLIC] about
*/

int meshTranslate(mesh_s *mesh, float x, float y, float z)
{

  long f, i;

  if (!mesh) { return -1; }

  /* Update each animation frame. */

  for (f = 0; f < mesh->num_frames; f++)
  {

    /* Update the AABB values. */

    mesh->frame[f]->xyzmin[0] += x; mesh->frame[f]->xyzmax[0] += x;
    mesh->frame[f]->xyzmin[1] += y; mesh->frame[f]->xyzmax[1] += y;
    mesh->frame[f]->xyzmin[2] += z; mesh->frame[f]->xyzmax[2] += z;

    /* Update the vertex positions. */

    for (i = 0; i < mesh->num_verts; i++)
    {
      mesh->vertex[f][(i*3)+0] += x;
      mesh->vertex[f][(i*3)+1] += y;
      mesh->vertex[f][(i*3)+2] += z;
    }

  }

  /* Update the joint positions. */

  if (mesh->tag_list)
  {

    for (i = 0; i < mesh->tag_list->num_tags; i++)
    {

      mesh->tag_list->tag[i]->origin[0] += x;
      mesh->tag_list->tag[i]->origin[1] += y;
      mesh->tag_list->tag[i]->origin[2] += z;

      mesh->tag_list->tag[i]->xyzmin[0] += x;
      mesh->tag_list->tag[i]->xyzmin[1] += y;
      mesh->tag_list->tag[i]->xyzmin[2] += z;

      mesh->tag_list->tag[i]->xyzmax[0] += x;
      mesh->tag_list->tag[i]->xyzmax[1] += y;
      mesh->tag_list->tag[i]->xyzmax[2] += z;

    }

  }

  return 0;

}

/*
[PUBLIC] about
*/

int meshGetCenter(const mesh_s *mesh, long frame, float *vec3)
{

  int i = 0;

  if (!mesh || !vec3) { return -1; }
  if (frame < 0 || frame >= mesh->num_frames) { return -2; }

  for (i = 0; i < 3; i++)
  {
    vec3[i] = mesh->frame[frame]->xyzmin[i] + ((mesh->frame[frame]->xyzmax[i] - mesh->frame[frame]->xyzmin[i]) / 2.0);
  }

  return 0;

}

/*
[PUBLIC] about
*/

int meshGetTagCenter(const mesh_s *mesh, long tag_id, float *vec3)
{

  int i = 0;

  if      (!mesh || !vec3 ) { return -1; }
  else if (!mesh->tag_list) { return -2; }
  else if (tag_id < 0 || tag_id >= mesh->tag_list->num_tags) { return -3; }

  for (i = 0; i < 3; i++)
  {
    vec3[i] = mesh->tag_list->tag[tag_id]->xyzmin[i] + ((mesh->tag_list->tag[tag_id]->xyzmax[i] - mesh->tag_list->tag[tag_id]->xyzmin[i]) / 2.0);
  }

  return 0;

}

/*
[PUBLIC] about
*/

int meshCreateBuffers(mesh_s *mesh)
{

  long i = 0;

  if (!mesh) { return -1; }

  /* Allocate a vertex buffer. This is shared by all groups within the mesh. */

  if (!mesh->final_vertex)
  {
    mesh->final_vertex = (float*)malloc(sizeof(float) * 3 * mesh->num_verts);
    if (!mesh->final_vertex) { return -2; }
    memset(mesh->final_vertex, 0, sizeof(float) * 3 * mesh->num_verts);
  }

  /* Allocate a normal buffer for each group individually. */

  for (i = 0; i < mesh->num_groups; i++)
  {

    mesh_group_s *group = mesh->group[i];
    size_t        length = 0;

    /* If memory has already been allocated, or doesn't need to be, skip it. */

    if      (!group                                    ) { continue; }
    else if (group->num_tris < 1 || group->final_normal) { continue; }

    /* Allocate an XYZ position for each vertex of each triangle. */

    length = 3*sizeof(float) * 3*group->num_tris;

    group->final_normal = (float*)malloc(length);

    if (!group->final_normal)
    {
      meshDeleteBuffers(mesh);
      return -3;
    }

    memset(group->final_normal, 0, length);

  }

  /* Report success. */

  return 0;

}

/*
[PUBLIC] about
*/

int meshDeleteBuffers(mesh_s *mesh)
{

  long i = 0;

  if (!mesh) { return -1; }

  /* Free the vertex buffer. */

  if (mesh->final_vertex)
  {
    free(mesh->final_vertex);
    mesh->final_vertex = NULL;
  }

  /* Free the normal buffer(s). */

  for (i = 0; i < mesh->num_groups; i++)
  {
    if (mesh->group[i])
    {
      if (mesh->group[i]->final_normal)
      {
        free(mesh->group[i]->final_normal);
        mesh->group[i]->final_normal = NULL;
      }
    }
  }

  /* Report success. */

  return 0;

}
