/*

FILENAME: mesh.h
AUTHOR/S: Thomas Dennis
CREATION: Sometime 2008

Copyright (c) 2008-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

#ifndef __QQQ_MESH_H__
#define __QQQ_MESH_H__

/**
@file mesh.h

@brief Provides utilities for loading and saving 3D model files.

Models may be animated using either "skeletal" transformations or "morph-based"
frame-by-frame sequences. Non-animated formats can be thought of as morph-based
models with only one frame of animation.

@todo Implement PLY import.

@todo Support IQM: http://sauerbraten.org/iqm/iqm.txt

@todo Support IQE: http://sauerbraten.org/iqm/iqe.txt

@todo Support 3DS and LWO. LWO files have skeleton data, which is useful.

@todo Support the various *.bsp formats used by Quake and Half-Life.
*/

#ifdef __cplusplus
extern "C" {
#endif

#include "mtrl.h"
#include "util/quaternion.h"

/* Constants. */

#define MESH_MAXTAGNAME 64 /**< @todo Use dynamic memory allocation instead. */

/**
@brief Defines a single MD3 tag, MS3D bone, or MD5Mesh joint.

This structure is generally referred to as a "tag" throughout the code for two
reasons: (A) MD3 support was implemented first, and (B) "joint" and "bone" are
sometimes a bit misleading. For example, MilkShape3D "bones" are actually more
like joints most of the time, but "root" (or "parentless") bones aren't, since
they don't connect the "child" to anything else.
*/

typedef struct
{

  char name[MESH_MAXTAGNAME]; /**< @brief A unique name string for this tag. */
  long parent;                /**< @brief The "parent" tag index; -1 = None. */

  vector3_t    origin;      /**< @brief The XYZ position of the tag. */
  quaternion_t quaternion;  /**< @brief The orientation of this tag. */

  vector3_t xyzmin; /**< @brief AABB data; defines the minimum XYZ position. */
  vector3_t xyzmax; /**< @brief AABB data; defines the maximum XYZ position. */

} mesh_tag_s;

/**
@brief Stores a list of tag structures.

For skeletal meshes, this can be used to represent a single frame of animation.
*/

typedef struct
{

  long num_tags;    /**< @brief The number of tags in the tag[] list. */
  mesh_tag_s **tag; /**< @brief An array of mesh_tag_s structures.    */

} mesh_tag_set_s;

/**
@brief Provides useful information about a single animation frame.

Note that this describes a single MORPH-BASED frame; For skeletal formats, this
will describe the "default" position of the mesh, ignoring any skeleton data.
*/

typedef struct
{

  char *name; /**< @brief Name of this animation frame. */

  vector3_t xyzmin; /**< @brief AABB data; defines the minimum XYZ position. */
  vector3_t xyzmax; /**< @brief AABB data; defines the maximum XYZ position. */

} mesh_frame_s;

/**
@brief Describes a single triangle within a mesh.

Note that the same vertex and normal indices are used for all animation frames
of a morph-animated mesh. The vertices may change positions between frames, but
the indices used to reference them do not.
*/

typedef struct
{

  long v_id[3]; /**< @brief Vertex index for each point of the triangle.   */
  long n_id[3]; /**< @brief Normal index for each point of the triangle.   */
  long t_id[3]; /**< @brief Texcoord index for each point of the triangle. */

} mesh_tri_s;

/**
@brief Describes a single group (or "object", or "surface") within a mesh file.

Each group has its own list of triangles, but the vertex, normal, and tex-coord
arrays are shared by all groups in the model.
*/

typedef struct
{

  char *name;   /**< @brief The (unique) name for this group. */
  long mat_id;  /**< @brief Material ID used, or -1 for none. */

  long num_tris;    /**< @brief The number of triangles in the group. */
  mesh_tri_s **tri; /**< @brief An array of triangle data structures. */

  float *final_normal;  /**< @brief OPTIONAL: final_normal[num_tris * 3 * 3] */

} mesh_group_s;

/**
@brief The main mesh structure.

This is used to contain everything else.
*/

typedef struct
{

  mtrl_list_s *mtrl;    /**< @brief A list of materials used by this mesh. */

  long num_frames;      /**< @brief Number of morph-based animation frames. */
  mesh_frame_s **frame; /**< @brief Frame data for (morph-based) animation. */

  long num_verts;       /**< @brief The total number of vertices.   */
  float **vertex;       /**< @brief vertex[num_frames][num_verts*3] */

  long num_norms;       /**< @brief The total number of normals.    */
  float **normal;       /**< @brief normal[num_frames][num_norms*3] */

  long num_coords;      /**< @brief The total number of texture coordinates. */
  float *texcoord;      /**< @brief texcoord[num_coords*2]                   */

  long num_groups;      /**< @brief The number of groups within this mesh. */
  mesh_group_s **group; /**< @brief group[num_groups]                      */

  mesh_tag_set_s *tag_list; /**< @brief Default tag positions, if required.  */
  long           *vert_tag; /**< @brief Vertex->Tag IDs: joint_id[num_verts] */

  float *final_vertex;  /**< @brief OPTIONAL: final_vertex[num_verts * 3] */

} mesh_s;

/**
@brief Load a mesh file from a text string.

This function only attempts to handle plain-text formats, such as OBJ or PLY.

@param text The text buffer to parse.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *morphLoadText(const char *text);

/**
@brief Load a mesh file from a binary data buffer.

This function does not attempt to handle plain-text formats such as OBJ or PLY.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *morphLoadData(long length, const unsigned char *data);

/**
@brief Load a mesh from a file.

This is just a convenient wrapper for morphLoadData() and morphLoadText().

@param file_name The file name to load.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *morphLoadFile(const char *file_name);

/**
@brief Export a mesh as an STL-format file.

Currently, only binary STL files are supported.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int morphSaveSTL(const mesh_s *mesh, long frame, const char *file_name);

/**
@brief Export a mesh as a Wavefront OBJ-format file.

This can also be used to export the material data in MTL format.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param obj_file The OBJ file name to export to.
@param mtl_file The MTL file name to export to.
@return Zero on success, or non-zero on error.
*/

int morphSaveOBJ(const mesh_s *mesh, long frame, const char *obj_file,
                                                       const char *mtl_file);

/**
@brief Export a mesh as a Princeton OFF (Object File Format) file.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int morphSaveOFF(const mesh_s *mesh, long frame, const char *file_name);

/**
@brief Export a mesh as a Stanford PLY file.

@param mesh The mesh to be exported.
@param frame The frame of animation to export.
@param file_name The file name to export to.
@return Zero on success, or non-zero on error.
*/

int morphSavePLY(const mesh_s *mesh, long frame, const char *file_name);

/**
@brief Free a previously created mesh from memory.

NULL pointers are ignored.

@param mesh The mesh to be freed.
@return Always returns NULL.
*/

mesh_s *morphFree(mesh_s *mesh);

/**
@brief Re-calculate the AABB information of a mesh.

This only re-calculates the frame AABB data; skeletal AABB data is not updated.

@param mesh The mesh structure to update.
@return Zero on success, or non-zero on error.
*/

int morphCalculateAABB(mesh_s *mesh);

/**
@brief Re-calculate the AABB information of a mesh skeleton.

@todo Explain more here...

@param mesh The mesh structure to update.
@param frame The animation frame index to use when calculating the AABBs.
@return Zero on success, or non-zero on error.
*/

int morphCalculateTagAABB(mesh_s *mesh, long frame);

/**
@brief Invert the normals of a mesh.

@todo Write more here.

@param mesh The mesh to be modified.
@return Zero on success, or non-zero on failure.
*/

int morphInvertNormals(mesh_s *mesh);

/**
@brief Reverse all faces in a mesh.

Note that this will also invert the normal data.

@param mesh The mesh to reverse the faces of.
@return Zero on success, or non-zero on failure.
*/

int morphReverseFaces(mesh_s *mesh);

/**
@brief about

writeme

@param mesh about
@param x about
@param y about
@param z about
@return Zero on success, or non-zero on failure.
*/

int meshScale(mesh_s *mesh, float x, float y, float z);

/**
@brief about

writeme

@param mesh about
@param x about
@param y about
@param z about
@return Zero on success, or non-zero on failure.
*/

int meshTranslate(mesh_s *mesh, float x, float y, float z);

/**
@brief about

writeme

@param mesh about
@param frame about
@param vec3 about
@return Zero on success, or non-zero on failure.
*/

int meshGetCenter(const mesh_s *mesh, long frame, float *vec3);

/**
@brief about

writeme

@param mesh about
@param tag_id about
@param vec3 about
@return Zero on success, or non-zero on failure.
*/

int meshGetTagCenter(const mesh_s *mesh, long tag_id, float *vec3);

/**
@brief about

writeme

@param mesh about
@return writeme
*/

int meshCreateBuffers(mesh_s *mesh);

/**
@brief about

writeme

@param mesh about
@return writeme
*/

int meshDeleteBuffers(mesh_s *mesh);

/**
@brief about

writeme

@param num_tags about
@return writeme
*/

mesh_tag_set_s *meshTagSetCreate(long num_tags);

/**
@brief about

writeme

@param frame about
@return writeme
*/

mesh_tag_set_s *meshTagSetDelete(mesh_tag_set_s *frame);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_MESH_H__ */
