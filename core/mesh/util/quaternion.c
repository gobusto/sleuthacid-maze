/*

FILENAME: quaternion.c
AUTHOR/S: Thomas Dennis
CREATION: 17th September 2013

Copyright (C) 2013 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file quaternion.c

@brief Provides various utilities for working with quaternions.

writeme
*/

#include <math.h>
#include "quaternion.h"

/* Constants and macros. */

#define SQ(x) (x*x) /**< @brief This is just to make the code more readable. */

/*
[PUBLIC] Get the length of a vector.
*/

float vecLength(int d, const float *v)
{

  float length = 0;
  int   i      = 0;

  if (!v || d < 1) { return 0; }

  for (i = 0; i < d; i++) { length += v[i] * v[i]; }

  return sqrt(length);

}

/*
[PUBLIC] Calculate the W-component of a unit-length quaternion.

REFERENCE:

http://tfc.duke.free.fr/coding/md5-specs-en.html
*/

float quatCalculateW(float x, float y, float z)
{
  float t = 1.0 - SQ(x) - SQ(y) - SQ(z);
  return (t < 0) ? 0 : -sqrt(t);
}

/*
[PUBLIC] Initialise a quaternion to an identity state.
*/

int quatIdentity(quaternion_t quaternion)
{

  if (!quaternion) { return -1; }

  quaternion[QUAT_W] = 1.0;
  quaternion[QUAT_X] = 0.0;
  quaternion[QUAT_Y] = 0.0;
  quaternion[QUAT_Z] = 0.0;

  return 0;

}

/*
[PUBLIC] Convert a single euler angle (specified in radians) into a quaternion.

The more general formula is as follows:

qw = cos(angle/2)
qx = ax * sin(angle/2)
qy = ay * sin(angle/2)
qz = az * sin(angle/2)

REFERENCE:

http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
*/

int quatFromAngle(quaternion_t quaternion, axis_t axis, float angle)
{

  if (!quaternion || axis == QUAT_W) { return -1; }

  quaternion[QUAT_W] = cos(angle / 2.0);
  quaternion[QUAT_X] = (axis == QUAT_X) ? sin(angle / 2.0) : 0.0;
  quaternion[QUAT_Y] = (axis == QUAT_Y) ? sin(angle / 2.0) : 0.0;
  quaternion[QUAT_Z] = (axis == QUAT_Z) ? sin(angle / 2.0) : 0.0;

  return 0;

}

/*
[PUBLIC] Convert three XYZ euler angles into a quaternion.

The Z/Y/X quaternion multiplication order is based on the MS3D file format.

REFERENCE:

http://www.flipcode.com/documents/matrfaq.html#Q60

http://content.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation#Quaternion_from_Euler_angles
*/

int quatFromEuler(quaternion_t output, float x, float y, float z)
{

  quaternion_t qx, qy, qz, temp;

  if (!output) { return -1; }

  quatFromAngle(qx, QUAT_X, x);
  quatFromAngle(qy, QUAT_Y, y);
  quatFromAngle(qz, QUAT_Z, z);

  quatMultiply(temp, qz, qy);
  quatMultiply(output, temp, qx);

  return 0;

}

/*
[PUBLIC] Invert a quaternion i.e. negate the X/Y/Z components.
*/

int quatInvert(quaternion_t quaternion)
{

  if (!quaternion) { return -1; }

  quaternion[QUAT_X] *= -1;
  quaternion[QUAT_Y] *= -1;
  quaternion[QUAT_Z] *= -1;

  return 0;

}

/*
[PUBLIC] Multiply one quaternion by another.
*/

int quatMultiply(quaternion_t q3, const quaternion_t q1, const quaternion_t q2)
{

  if (!q3 || !q1 || !q2) { return -1; }

  q3[QUAT_W] = q1[QUAT_W]*q2[QUAT_W] - q1[QUAT_X]*q2[QUAT_X] - q1[QUAT_Y]*q2[QUAT_Y] - q1[QUAT_Z]*q2[QUAT_Z];
  q3[QUAT_X] = q1[QUAT_W]*q2[QUAT_X] + q1[QUAT_X]*q2[QUAT_W] + q1[QUAT_Y]*q2[QUAT_Z] - q1[QUAT_Z]*q2[QUAT_Y];
  q3[QUAT_Y] = q1[QUAT_W]*q2[QUAT_Y] - q1[QUAT_X]*q2[QUAT_Z] + q1[QUAT_Y]*q2[QUAT_W] + q1[QUAT_Z]*q2[QUAT_X];
  q3[QUAT_Z] = q1[QUAT_W]*q2[QUAT_Z] + q1[QUAT_X]*q2[QUAT_Y] - q1[QUAT_Y]*q2[QUAT_X] + q1[QUAT_Z]*q2[QUAT_W];

  return 0;

}

/*
[PUBLIC] Rotate an XYZ position using a quaternion.
*/

int quatRotate(quaternion_t output, const quaternion_t p, quaternion_t r)
{

  quaternion_t temp;

  if (!output || !p || !r) { return -1; }

  quatMultiply(temp, r, p);
  quatInvert(r);
  quatMultiply(output, temp, r);
  quatInvert(r);

  return 0;

}
