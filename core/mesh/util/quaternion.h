/*

FILENAME: quaternion.h
AUTHOR/S: Thomas Dennis
CREATION: 17th September 2013

Copyright (C) 2013 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file quaternion.h

@brief Provides various utilities for working with quaternions.

writeme
*/

#ifndef __QQQ_QUATERNION_H__
#define __QQQ_QUATERNION_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Constants and macros. */

#define QQQ_PI 3.14159265358979323846 /**< @brief Pi.                       */
#define RAD2DEG (180.0 / QQQ_PI)      /**< @brief Radians-to-degrees value. */

/**
@brief Enumerates each possible 3D axis: X, Y, and Z.

A "W" value is included so that the indices match up with the quaternion order.
*/

typedef enum
{

  QUAT_W, /**< @brief The W component of the quaternion; not really an axis. */
  QUAT_X, /**< @brief The X component of the 3D directional vector. */
  QUAT_Y, /**< @brief The Y component of the 3D directional vector. */
  QUAT_Z  /**< @brief The Z component of the 3D directional vector. */

} axis_t;

/* Datatypes. */

typedef float vector3_t[3];     /**< @brief A 3D XYZ vector.              */
typedef float quaternion_t[4];  /**< @brief A basic WXYZ quaternion type. */

/**
@brief Get the length of an n-dimensional vector.

This can also be used to get the length of a quaternion.

@param d The number of dimensions that the vector has.
@param v The vector values.
@return The length of the vector.
*/

float vecLength(int d, const float *v);

/**
@brief Calculate the W-component of a unit-length quaternion.

This is useful for loading Doom 3 .md5mesh files.

@param x The X component of the 3D directional vector.
@param y The Y component of the 3D directional vector.
@param z The Z component of the 3D directional vector.
@return The calculated quaternion W component.
*/

float quatCalculateW(float x, float y, float z);

/**
@brief Initialise a quaternion to an identity state.

An identity quaternion will be set to W=1, X=0, Y=0, Z=0, i.e. no rotation.

@param quaternion The quaternion to be set.
@return Zero on success, or non-zero on failure.
*/

int quatIdentity(quaternion_t quaternion);

/**
@brief Convert a single euler angle (specified in radians) into a quaternion.

The axis parameter should be QUAT_X, QUAT_Y, or QUAT_Z; QUAT_W is not valid.

@param quaternion The quaternion to be set.
@param axis The axis about which to rotate.
@param angle The angle to use, in radians.
@return Zero on success, or non-zero on failure.
*/

int quatFromAngle(quaternion_t quaternion, axis_t axis, float angle);

/**
@brief Convert three XYZ euler angles into a quaternion.

This is useful for loading MilkShape3D .ms3d files.

@param output The quaternion to be set.
@param x The X euler angle in radians.
@param y The Y euler angle in radians.
@param z The Z euler angle in radians.
@return Zero on success, or non-zero on failure.
*/

int quatFromEuler(quaternion_t output, float x, float y, float z);

/**
@brief Invert a quaternion i.e. negate the X/Y/Z components.

Note that, unlike some other functions, this modifies the quaternion in-place.

@param quaternion The quaternion to be inverted.
@return Zero on success, or non-zero on failure.
*/

int quatInvert(quaternion_t quaternion);

/**
@brief Multiply one quaternion by another.

To rotate a vector about a (unit-length) rotation quaternion, do the following:

quatMultiply(temp, rotation_quaternion, wxyz);

quatInvert(rotation_quaternion);

quatMultiply(wxyz, temp, rotation_quaternion);

...where the wxyz quaternion is a 3D XYZ positional vector, with W set to zero.

@param q3 The quaternion used to store the result of the multiplication.
@param q1 The first quaternion.
@param q2 The second quaternion.
@return Zero on success, or non-zero on failure.
*/

int quatMultiply(quaternion_t q3, const quaternion_t q1, const quaternion_t q2);

/**
@brief Rotate an XYZ position using a quaternion.

This is provided for convenience - see quatMultiply() for details.

@param output A "result" quaternion, used to store the final (rotated) position.
@param p A "position" quaternion, i.e. An XYZ position with a W-value of zero.
@param r A "rotation" quaternion, i.e a unit-length WXYZ value.
@return Zero on success, or non-zero on failure.
*/

int quatRotate(quaternion_t output, const quaternion_t p, quaternion_t r);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_QUATERNION_H__ */
