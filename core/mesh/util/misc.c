/*

FILENAME: misc.c
AUTHOR/S: Thomas Dennis
CREATION: 10th March 2012

Copyright (c) 2012-2013 Thomas Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

/**
@file misc.c

@brief Contains various things which are useful in multiple parts of the code.

This file is mostly for internal use by the mesh-loader code, but it's okay for
it to be used externally too.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "misc.h"

/*
[PUBLIC] Break a string of text into individual lines and handle each in turn.
*/

int txtEachLine(const char *text, const char *comment,
                int (*user_func)(const char*, void*), void *user_data)
{

  char *line = NULL;
  int read_mode = 0;
  size_t comment_length, i;

  if (!text || !user_func) { return 0; }

  comment_length = comment ? strlen(comment) : 0;

  for (read_mode = 0; text[0];)
  {

    /* If read_mode is false, then skip any preceding whitespace characters. */
    if (!read_mode)
    {
      if (isspace(text[0])) { text++; } else { read_mode = 1; i = 0; }
    }
    else
    {

      /* If a comment is found, treat it as a pseudo-EOL character. */
      if (comment)
      {
        if (strncmp(&text[i], comment, comment_length) == 0) { read_mode = 0; }
      }

      /* If an EOL is found, handle everything found up until this point. */
      if (text[i] == '\n' || text[i] == '\r' || text[i] == 0 || !read_mode)
      {

        /* Trim any trailing whitespace. */
        while (i > 0 && isspace(text[i-1])) { i--; }

        /* Comments at the start of a line can lead to zero-length lines. */
        if (i > 0)
        {

          int result = 0;
          size_t c = 0;

          /* Allocate memory for the text + a NULL terminator byte. */
          line = (char*)malloc(i + 1);
          if (!line) { return 666; }
          line[i] = 0;

          /* Copy the text, converting any tab characters into spaces. */
          for (c = 0; c < i; c++) { line[c] = text[c] == '\t' ? ' ' : text[c]; }

          /* Pass the current line to the callback function and then free it. */
          result = user_func(line, user_data);
          free(line);

          /* If something goes wrong, stop here and return the error value. */
          if (result != 0) { return result; }

        }

        /* If the "EOL" was actually a comment, look for a real EOL instead. */
        while (text[0] != '\n' && text[0] != '\r' && text[0] != 0) { text++; }

        /* Go back to searching for the start of a new line. */
        read_mode = 0;

      } else { i++; }

    }

  }

  /* Report success. */
  return 0;

}

/*
[PUBLIC] Load the contents of a file into a malloc()'d buffer.
*/

unsigned char *txtLoadFile(const char *file_name, long *length)
{

  FILE          *fsrc = NULL;
  unsigned char *data = NULL;
  long          size  = 0;

  /* Attempt to open the requested file. */

  fsrc = fopen(file_name, "rb");
  if (!fsrc) { return NULL; }

  /* Get the length of the file in bytes. */

  fseek(fsrc, 0, SEEK_END);
  size = (long)ftell(fsrc);
  rewind(fsrc);

  /* Copy the data into memory (with an extra zero byte, in case it's text). */

  data = (unsigned char*)malloc(size + 1);

  if (data)
  {
    memset(data, 0, size + 1);
    fread(data, 1, size, fsrc);
  }

  fclose(fsrc);

  /* Return the result. */

  if (length) { *length = size; }

  return data;

}
