/**
@file qqqGraphics_GL1.c

@brief Contains graphics-related functionality.

This file implements the functions declared in qqqGraphics.h using OpenGL 1.x.
*/

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "qqqGraphics.h"
#include "qqqWorld.h"
#include "bitmap/bitmap.h"
#include "mesh/mesh.h"

/* Global objects and variables. */

int gfx_w = 640;  /**< @brief Display width in pixels.  */
int gfx_h = 480;  /**< @brief Display height in pixels. */

GLuint gfx_sprites = 0;     /**< @brief Ths sprite sheet for all 2D images.  */
mesh_s *gfx_3Dmesh = NULL;  /**< @brief The 3D mesh drawn in the background. */

/**
@brief [INTERNAL] Load an image file as an OpenGL texture.

@param file_name The filename to be loaded.
@return A valid GLuint texture ID on success or zero on failure.
*/

static GLuint gfxLoadTexture(const char *file_name)
{

  bmp_image_s *image = NULL;
  GLuint gl_texture = 0;
  GLenum format;

  /* Attempt to load the image file. */

  image = bmpLoadFile(file_name);

  if (!image) { return 0; }

  switch (image->bpp)
  {
    case 32: format = GL_RGBA;            break;    /* RGB with alpha.       */
    case 24: format = GL_RGB;             break;    /* RGB only.             */
    case 16: format = GL_LUMINANCE_ALPHA; break;    /* Greyscale with alpha. */
    case 8:  format = GL_LUMINANCE;       break;    /* Greyscale only.       */
    default: bmpFree(image);              return 0; /* Unknown pixel format. */
  }

  /* Create an OpenGL texture and copy the pixel data into it. */

  glGenTextures(1, &gl_texture);
  glBindTexture(GL_TEXTURE_2D, gl_texture);

  gluBuild2DMipmaps(GL_TEXTURE_2D, image->bpp / 8, image->w, image->h, format, GL_UNSIGNED_BYTE, image->data);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  /* Free the bitmap data structure and return the OpenGL texture. */

  bmpFree(image);
  return gl_texture;

}

/*
[PUBLIC] Initialise the graphics subsystem.
*/

int gfxInit(int w, int h)
{

  gfxQuit();

  if (w < 1 || h < 1) { return -1; }

  gfx_w = w;
  gfx_h = h;

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0.0);

  gfx_sprites = gfxLoadTexture("images/tomd/sprites.tga");
  gfx_3Dmesh = morphLoadFile("models/noway/camel.obj");

  return 0;

}

/*
[PUBLIC] Shut down the graphics subsystem.
*/

void gfxQuit(void)
{

  glDeleteTextures(1, &gfx_sprites);

  morphFree(gfx_3Dmesh);
  gfx_3Dmesh = NULL;

}

/**
@brief [INTERNAL] Draw a 3D mesh.

@param mesh The mesh to be drawn.
*/

static void gfxDrawMesh(const mesh_s *mesh)
{

  long g, t, v;

  if (!mesh) { return; }

  glBegin(GL_TRIANGLES);

  for (g = 0; g < mesh->num_groups; g++)
  {

    mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    for (t = 0; t < group->num_tris; t++)
    {

      for (v = 0; v < 3; v++)
      {

        glVertex3f(
            mesh->vertex[0][(group->tri[t]->v_id[v]*3) + 0],
            mesh->vertex[0][(group->tri[t]->v_id[v]*3) + 1],
            mesh->vertex[0][(group->tri[t]->v_id[v]*3) + 2]);

      }

    }

  }

  glEnd();

}

/**
@brief [INTERNAL] Draw a string of characters on-screen.

This doesn't handle all ASCII characters, just numbers 0-9. Some special values
are mapped to fixed positions on the sprite sheet for rendering game objects.

@param x The X position of the text.
@param y The Y position of the text.
@param text A null-terminated (C-style) string of text.
*/

static void gfxDrawText(int x, int y, const char *text)
{

  float tx, ty;
  size_t i = 0;

  if (!text) { return; }

  glBegin(GL_QUADS);

  for (i = 0; text[i]; ++i)
  {

    switch(text[i])
    {

      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9':
        tx =    ((text[i] - '0') % 4)  * 0.25;
        ty = (3-((text[i] - '0') / 4)) * 0.25;
      break;

      case '$': tx = 0.50; ty = 0.25; break;
      case '&': tx = 0.75; ty = 0.25; break;
      case '#': tx = 0.25; ty = 0.00; break;
      case 'o': tx = 0.50; ty = 0.00; break;
      case 'O': tx = 0.75; ty = 0.00; break;
      default:  tx = 0.00; ty = 0.00; break;

    }

    glTexCoord2f(tx+0.00, ty+0.25); glVertex2f(x+i+0, y+0);
    glTexCoord2f(tx+0.25, ty+0.25); glVertex2f(x+i+1, y+0);
    glTexCoord2f(tx+0.25, ty+0.00); glVertex2f(x+i+1, y+1);
    glTexCoord2f(tx+0.00, ty+0.00); glVertex2f(x+i+0, y+1);

  }

  glEnd();

}

/*
[PUBLIC] Redraw the display.
*/

void gfxDraw(double ticks)
{

  char strscore[32];
  int x, y;

  /* Clear the display. */

  glClearColor(0.5, 0.5, 0.25, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  /* Prepare to render the 3D background. */

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(55.0, (float)gfx_w / (float)gfx_h, 1.0, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glScissor(16, 48, 16+(16*31), 16+(16*31));

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glEnable(GL_SCISSOR_TEST);
  glDisable(GL_TEXTURE_2D);

  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);

  for (x = 0; x < 5; ++x)
  {

    double timer = ticks + (x * 400);
    double scale = 1.0 + (sin(timer / 500.0) * 0.4);

    glColor3f((sin(timer / 500.0) / 4.0 + 0.25) * ((x+1)*0.18),
              (cos(timer / 500.0) / 4.0 + 0.25) * ((x+1)*0.18),
              0.5                               * ((x+1)*0.18));

    glPushMatrix();

    glTranslatef(0, 0, -5);
    glScalef(scale, scale, scale);
    glRotatef(timer / 75.0, 1, 0, -1);

    gfxDrawMesh(gfx_3Dmesh);

    glPopMatrix();

  }

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDisable(GL_SCISSOR_TEST);
  glEnable(GL_TEXTURE_2D);

  /* Prepare to render the 2D foreground. */

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, gfx_w, gfx_h, 0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glScalef(16, 16, 1);

  glColor3ub(255, 255, 255);

  /* Draw the maze tiles. */

  for (y = 0; y < worldGetHeight(); ++y)
  {

    for (x = 0; x < worldGetWidth(); ++x)
    {

      const char *c = " ";

      switch (worldGetTile(x, y))
      {
        case SAM_TILE_WALL: c = "#"; break;
        case SAM_TILE_ROCK: c = "o"; break;
        case SAM_TILE_HOLE: c = "O"; break;
        case SAM_TILE_NONE: case SAM_NUM_TILES: default: break;
      }

      gfxDrawText(1 + x, 1 + y, c);

    }

  }

  /* Draw the player. */

  gfxDrawText(1 + worldGetX(), 1 + worldGetY(), "&");

  /* Draw the score. */

  sprintf(strscore, "$%d", worldGetScore());
  gfxDrawText(1, 34, strscore);

}
