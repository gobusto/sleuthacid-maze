/**
@file qqqWorld.c

@brief Contains the main game logic functionality.

All game logic, physics, etc. belongs here.
*/

#include <stdlib.h>
#include "qqqAudio.h"
#include "qqqWorld.h"

/* Constants. */

#define SAM_MAP_W 32  /**< @brief The width of the map in tiles.  */
#define SAM_MAP_H 32  /**< @brief The height of the map in tiles. */

/* Global objects and variables. */

sam_tile_t maze[SAM_MAP_W][SAM_MAP_H];  /**< @brief The map tile data. */

/**
@brief The player data structure.

There's currently only ever one of these, but there could be multiple instances
in future if a multiplayer option was implemented...
*/

struct
{

  unsigned char xpos; /**< @brief Player tile X position. */
  unsigned char ypos; /**< @brief Player tile Y position. */
  unsigned int score; /**< @brief Playr score.            */

} player;

/*
[PUBLIC] Generate a new world.
*/

int worldInit(void)
{

  int x, y;

  worldQuit();

  for (y = 0; y < SAM_MAP_H; ++y)
  {
    for (x = 0; x < SAM_MAP_W; x++)
    {
      maze[x][y] = (rand() & 7) - 4;
      if (maze[x][y] >= SAM_NUM_TILES) { maze[x][y] = SAM_TILE_NONE; }
    }
  }

  maze[0][0] = 0;

  player.xpos  = 0;
  player.ypos  = 0;
  player.score = 0;

  sndPlayEffect(SND_EFFECT_WARP);

  return 0;

}

/*
[PUBLIC] Free any currently existing world data.
*/

void worldQuit(void)
{
  /* Currently doesn't do anything... */
}

/**
@brief [INTERNAL] Move a rock tile to a new location.

@param cellx The original X tile position of the rock.
@param celly The original Y tile position of the rock.
@param xdelta The number of X tiles to move, relative to the current position.
@param ydelta The number of Y tiles to move, relative to the current position.
@return Zero if the rock could not be moved, or non-zero if it was.
*/

static int worldMoveRock(int cellx, int celly, int xdelta, int ydelta)
{

  int xmove = (cellx + xdelta) & 31;
  int ymove = (celly + ydelta) & 31;

  switch (maze[xmove][ymove])
  {

    case SAM_TILE_NONE:
      maze[xmove][ymove] = SAM_TILE_ROCK;
      maze[cellx][celly] = SAM_TILE_NONE;
    return 1;

    case SAM_TILE_HOLE:
      maze[xmove][ymove] = SAM_TILE_NONE;
      maze[cellx][celly] = SAM_TILE_NONE;
      player.score++;
      sndPlayEffect(SND_EFFECT_GOAL);
    return 1;

    case SAM_TILE_WALL: case SAM_TILE_ROCK: case SAM_NUM_TILES: default: break;

  }

  return 0;

}

/*
[PUBLIC] Move the player character.
*/

void worldMovePlayer(int xdelta, int ydelta)
{

  int xmove = (player.xpos + xdelta) & 31;
  int ymove = (player.ypos + ydelta) & 31;

  switch (maze[xmove][ymove])
  {

    case SAM_TILE_NONE:
    case SAM_TILE_HOLE:
      player.xpos = xmove;
      player.ypos = ymove;
    break;

    case SAM_TILE_ROCK:
      if (worldMoveRock(xmove, ymove, xdelta, ydelta))
      {
        player.xpos = xmove;
        player.ypos = ymove;
      }
    break;

    case SAM_TILE_WALL: case SAM_NUM_TILES: default: break;

  }

}

/*
[PUBLIC] Get the width of the currently active world in tiles.
*/

int worldGetWidth(void) { return SAM_MAP_W; }

/*
[PUBLIC] Get the height of the currently active world in tiles.
*/

int worldGetHeight(void) { return SAM_MAP_H; }

/*
[PUBLIC] Get the tile at the coordinates specified.
*/

sam_tile_t worldGetTile(int x, int y)
{
  if (x >= 0 && y >= 0 && x < SAM_MAP_W && y < SAM_MAP_H) { return maze[x][y]; }
  return SAM_TILE_NONE;
}

/*
[PUBLIC] Get the player X position in tiles.
*/

int worldGetX(void) { return player.xpos; }

/*
[PUBLIC] Get the player Y position in tiles.
*/

int worldGetY(void) { return player.ypos; }

/*
[PUBLIC] Get the current score.
*/

int worldGetScore(void) { return player.score; }
